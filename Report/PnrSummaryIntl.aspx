﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PnrSummaryIntl.aspx.vb"
    Inherits="FlightInt_PnrSummaryIntl" EnableViewStateMac="false" ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register Src="~/DMT-Manager/User_Control/PnrSummaryIntl.ascx" TagPrefix="uc1" TagName="PnrSummaryIntl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="css/transtour.css" rel="stylesheet" type="text/css" />
    <link href="css/core_style.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/itz.css" rel="stylesheet" />
    <link href="../CSS/newcss/main.css" rel="stylesheet" />
    <link href="../CSS/foundation.css" rel="stylesheet" />

    <script src='../Hotel/JS/jquery-1.3.2.min.js' type='text/javascript'></script>
    <script src='../Hotel/JS/jquery-barcode.js' type='text/javascript'></script>

    <script type="text/javascript">
        function myPrint() {
            window.print();
        }
    </script>

    <script type="text/javascript" language='javascript'>
        function callprint(strid) {

            //var prtContent11 = document.getElementById("test");

            //var prtContent = document.getElementById(strid);

            var prtContent = $('#' + strid);
            var sst = '<html><head><title>Ticket Details</title><link rel="stylesheet" href="http://RWT.co/CSS/itz.css" type="text/css" media="print"></style></head><body>';

            var WinPrint = window.open('', '', 'left=0,top=0,width=750,height=500,toolbar=0,scrollbars=0,status=0');

            WinPrint.document.write('<html><head><title>Ticket Details</title>');

            WinPrint.document.write('</head><body>' + prtContent.html() + '</body></html>');


            ////prtContent11.innerHTML = sst + prtContent.innerHTML + "</body></html>";
            //WinPrint.document.write(prtContent11.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
            //prtContent.innerHTML = strOldOne;
        }
    </script>



    <style type="text/css">
        @media print {
            .pri {
                background-color: #0f4da2 !important;
                -webkit-print-color-adjust: exact;
            }
        }

        @media print {
            .pri font, .pri {
                color: white !important;
            }
        }

        @media print {
            .pri2 {
                background-color: #f1f1f1 !important;
                -webkit-print-color-adjust: exact;
            }
        }
    </style>



    <style type="text/css">
        @media print {
            body * {
                visibility: hidden;
            }

            #divprint, #divprint * {
                visibility: visible;
            }

            #divprint {
                position: absolute;
                left: 0;
                top: 0;
            }
        }

        .style1 {
            height: 14px;
        }

        .style2 {
            width: 40%;
        }

        * {
            box-sizing: border-box;
        }


        .share {
            position: fixed;
            left: 0;
            top: 25%;
            list-style-type: none;
            margin: 0;
            padding: 0;
            -moz-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
        }

            .share li {
                position: relative;
            }

                .share li:nth-of-type(1) .social-link,
                .share li:nth-of-type(1) .social-link:hover {
                    background: #3B5998 url("https://cdn3.iconfinder.com/data/icons/flat-design-spreadsheet-set-4/24/export-to-word-512.png") 50% 50% no-repeat;
                    background-size: 25px auto;
                }

                .share li:nth-of-type(1) .nav-label {
                    -moz-transition: background 0.4s ease, -moz-transform 0.4s ease 0.1s;
                    -o-transition: background 0.4s ease, -o-transform 0.4s ease 0.1s;
                    -webkit-transition: background 0.4s ease, -webkit-transform 0.4s ease;
                    -webkit-transition-delay: 0s, 0.1s;
                    transition: background 0.4s ease, transform 0.4s ease 0.1s;
                    background: #1e2e4f;
                }

                .share li:nth-of-type(1) .social-link:hover .nav-label {
                    -moz-transition: -moz-transform 0.4s ease, background 0.4s ease 0.1s;
                    -o-transition: -o-transform 0.4s ease, background 0.4s ease 0.1s;
                    -webkit-transition: -webkit-transform 0.4s ease, background 0.4s ease;
                    -webkit-transition-delay: 0s, 0.1s;
                    transition: transform 0.4s ease, background 0.4s ease 0.1s;
                    background: #3B5998;
                }

                .share li:nth-of-type(2) .social-link,
                .share li:nth-of-type(2) .social-link:hover {
                    background: #4099FF url("https://cdn4.iconfinder.com/data/icons/web-ui-color/128/Mail-512.png") 50% 50% no-repeat;
                    background-size: 25px auto;
                }

                .share li:nth-of-type(2) .nav-label {
                    -moz-transition: background 0.4s ease, -moz-transform 0.4s ease 0.1s;
                    -o-transition: background 0.4s ease, -o-transform 0.4s ease 0.1s;
                    -webkit-transition: background 0.4s ease, -webkit-transform 0.4s ease;
                    -webkit-transition-delay: 0s, 0.1s;
                    transition: background 0.4s ease, transform 0.4s ease 0.1s;
                    background: #0065d9;
                }

                .share li:nth-of-type(2) .social-link:hover .nav-label {
                    -moz-transition: -moz-transform 0.4s ease, background 0.4s ease 0.1s;
                    -o-transition: -o-transform 0.4s ease, background 0.4s ease 0.1s;
                    -webkit-transition: -webkit-transform 0.4s ease, background 0.4s ease;
                    -webkit-transition-delay: 0s, 0.1s;
                    transition: transform 0.4s ease, background 0.4s ease 0.1s;
                    background: #4099FF;
                }

                .share li:nth-of-type(3) .social-link,
                .share li:nth-of-type(3) .social-link:hover {
                    background: #3B5998 url("http://b2brichatravels.in/Images/icons/Due.png") 50% 50% no-repeat;
                    background-size: 50px auto;
                }

                .share li:nth-of-type(3) .nav-label {
                    -moz-transition: background 0.4s ease, -moz-transform 0.4s ease 0.1s;
                    -o-transition: background 0.4s ease, -o-transform 0.4s ease 0.1s;
                    -webkit-transition: background 0.4s ease, -webkit-transform 0.4s ease;
                    -webkit-transition-delay: 0s, 0.1s;
                    transition: background 0.4s ease, transform 0.4s ease 0.1s;
                    background: #1e2e4f;
                }

                .share li:nth-of-type(3) .social-link:hover .nav-label {
                    -moz-transition: -moz-transform 0.4s ease, background 0.4s ease 0.1s;
                    -o-transition: -o-transform 0.4s ease, background 0.4s ease 0.1s;
                    -webkit-transition: -webkit-transform 0.4s ease, background 0.4s ease;
                    -webkit-transition-delay: 0s, 0.1s;
                    transition: transform 0.4s ease, background 0.4s ease 0.1s;
                    background: #3B5998;
                }

                .share li:nth-of-type(4) .social-link,
                .share li:nth-of-type(4) .social-link:hover {
                    background: #3B5998 url("http://b2brichatravels.in/Images/printericon.jpg") 50% 50% no-repeat;
                    background-size: 25px auto;
                }

                .share li:nth-of-type(4) .nav-label {
                    -moz-transition: background 0.4s ease, -moz-transform 0.4s ease 0.1s;
                    -o-transition: background 0.4s ease, -o-transform 0.4s ease 0.1s;
                    -webkit-transition: background 0.4s ease, -webkit-transform 0.4s ease;
                    -webkit-transition-delay: 0s, 0.1s;
                    transition: background 0.4s ease, transform 0.4s ease 0.1s;
                    background: #1e2e4f;
                }

                .share li:nth-of-type(4) .social-link:hover .nav-label {
                    -moz-transition: -moz-transform 0.4s ease, background 0.4s ease 0.1s;
                    -o-transition: -o-transform 0.4s ease, background 0.4s ease 0.1s;
                    -webkit-transition: -webkit-transform 0.4s ease, background 0.4s ease;
                    -webkit-transition-delay: 0s, 0.1s;
                    transition: transform 0.4s ease, background 0.4s ease 0.1s;
                    background: #3B5998;
                }

                /*======================*/
                .share li:nth-of-type(5) .social-link,
                .share li:nth-of-type(5) .social-link:hover {
                    background: #3B5998 url(http://b2brichatravels.in/Images/icons/Due.png) 50% 50% no-repeat;
                    background-size: 50px auto;
                }

                .share li:nth-of-type(5) .nav-label {
                    -moz-transition: background 0.4s ease, -moz-transform 0.4s ease 0.1s;
                    -o-transition: background 0.4s ease, -o-transform 0.4s ease 0.1s;
                    -webkit-transition: background 0.4s ease, -webkit-transform 0.4s ease;
                    -webkit-transition-delay: 0s, 0.1s;
                    transition: background 0.4s ease, transform 0.4s ease 0.1s;
                    background: #1e2e4f;
                }

                .share li:nth-of-type(5) .social-link:hover .nav-label {
                    -moz-transition: -moz-transform 0.4s ease, background 0.4s ease 0.1s;
                    -o-transition: -o-transform 0.4s ease, background 0.4s ease 0.1s;
                    -webkit-transition: -webkit-transform 0.4s ease, background 0.4s ease;
                    -webkit-transition-delay: 0s, 0.1s;
                    transition: transform 0.4s ease, background 0.4s ease 0.1s;
                    background: #3B5998;
                }
                /*======================*/
                .share li:nth-of-type(6) .social-link,
                .share li:nth-of-type(6) .social-link:hover {
                    background: #3B5998 url(../DMT-Manager/images/pdfimglogo.png) 50% 50% no-repeat;
                    background-size: 50px auto;
                }

                .share li:nth-of-type(6) .nav-label {
                    -moz-transition: background 0.4s ease, -moz-transform 0.4s ease 0.1s;
                    -o-transition: background 0.4s ease, -o-transform 0.4s ease 0.1s;
                    -webkit-transition: background 0.4s ease, -webkit-transform 0.4s ease;
                    -webkit-transition-delay: 0s, 0.1s;
                    transition: background 0.4s ease, transform 0.4s ease 0.1s;
                    background: #1e2e4f;
                }

                .share li:nth-of-type(6) .social-link:hover .nav-label {
                    -moz-transition: -moz-transform 0.4s ease, background 0.4s ease 0.1s;
                    -o-transition: -o-transform 0.4s ease, background 0.4s ease 0.1s;
                    -webkit-transition: -webkit-transform 0.4s ease, background 0.4s ease;
                    -webkit-transition-delay: 0s, 0.1s;
                    transition: transform 0.4s ease, background 0.4s ease 0.1s;
                    background: #3B5998;
                }

                .share li .social-link {
                    padding: 0;
                    display: block;
                    cursor: pointer;
                    width: 60px;
                    height: 60px;
                    padding: 15px 20px;
                }

                    .share li .social-link .nav-label {
                        font-family: sans-serif;
                        font-size: 14px;
                        color: white;
                        display: block;
                        height: 60px;
                        position: absolute;
                        top: 0px;
                        top: 0rem;
                        margin-left: 40px;
                        line-height: 64px;
                        padding: 0 20px;
                        white-space: nowrap;
                        z-index: 4;
                        -moz-transition: -moz-transform 0.4s ease;
                        -o-transition: -o-transform 0.4s ease;
                        -webkit-transition: -webkit-transform 0.4s ease;
                        transition: transform 0.4s ease;
                        -moz-transform-origin: left 50%;
                        -ms-transform-origin: left 50%;
                        -webkit-transform-origin: left 50%;
                        transform-origin: left 50%;
                        -moz-transform: rotateY(-90deg);
                        -webkit-transform: rotateY(-90deg);
                        transform: rotateY(-90deg);
                    }

                        .share li .social-link .nav-label span {
                            -moz-transform-origin: left 50%;
                            -ms-transform-origin: left 50%;
                            -webkit-transform-origin: left 50%;
                            transform-origin: left 50%;
                            -moz-transform: rotateY(-90deg);
                            -webkit-transform: rotateY(-90deg);
                            transform: rotateY(-90deg);
                        }

                    .share li .social-link:hover .nav-label,
                    .share li .social-link:hover .nav-label span {
                        -moz-transform: rotateY(0);
                        -webkit-transform: rotateY(0);
                        transform: rotateY(0);
                    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <ul class="share">
                <!--Facebook-->
                <li>
                    <div class="social-link">
                        <div class="nav-label" style="padding: 14px;">
                            <span>
                                <asp:Button ID="btn_exporttoword" runat="server" Text="ExportToWord" CssClass="buttonfltbks"
                                    CausesValidation="False" OnClientClick="return hidecharge();" /></span>
                        </div>
                    </div>
                </li>
                <!--Twitter-->
                <li>
                    <div class="social-link">
                        <div class="nav-label" style="padding: 14px; height: 111px; border-radius: 4px;">
                            <div class="row">
                                <div class="col-md-4">
                                    <asp:TextBox ID="txt_email" runat="server" CssClass="textboxflight" Style="width: 300px;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txt_email"
                                        ErrorMessage="*" ForeColor="#990000" Display="Dynamic">*</asp:RequiredFieldValidator>

                                    <asp:Button ID="btn" runat="server" OnClientClick="return emailvalidate();" CssClass="buttonfltbk" Text="Send"></asp:Button>

                                </div>
                            </div>
                            <div class="row">
                                <asp:Label ID="mailmsg" runat="server"></asp:Label>
                                <div style="text-align: center; color: #EC2F2F">
                                    <asp:RegularExpressionValidator ID="valRegEx" runat="server" ControlToValidate="txt_email"
                                        ValidationExpression=".*@.*\..*" ErrorMessage="*Invalid E-Mail ID." Display="dynamic">*Invalid E-Mail ID.</asp:RegularExpressionValidator>
                                </div>
                            </div>

                        </div>
                    </div>
                </li>
                <li>
                    <div class="social-link">
                        <div class="nav-label" style="padding: 14px;">
                            <div id="div1" runat="server">
                                <span>
                                    <input type="button" class="buttonfltbks" id="BtnShowFare" name="btn_edit" value="Fare Show" onclick="ShowFare();" style="display: none;" />&nbsp;
                                        <input type="button" class="buttonfltbks" id="BtnHideFare" name="btn_edit" value="Fare Hide" onclick="HideFare();" style="margin-top: -61px" />&nbsp;
                                        <asp:Button ID="btn_exporttoecel" runat="server" Text="ExportToExcel" BackColor="#004b91"
                                            Font-Bold="False" ForeColor="White" CausesValidation="False" OnClientClick="return hidecharge();"
                                            Visible="false" />
                                </span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="social-link">
                        <div class="nav-label" style="padding: 14px;">
                            <div style="width: 150px;">
                                <%--<a href='javascript:;' onclick='javascript:callprint("divprint");'>--%>
                                <a href='javascript:;' onclick="myPrint()">
                                    <img src='../Images/print_booking.jpg' border='0' alt="" style="height: 40px;" /></a>
                            </div>

                        </div>
                    </div>
                </li>
                <li>
                    <div class="social-link">
                        <div class="nav-label" style="padding: 14px;">
                            <div id="div5" runat="server">
                                <span>
                                    <input type="button" class="buttonfltbks" id="btnShowAgency" name="btn_edit" value="Agency Show" onclick="ShowAgency();" style="display: none;" />&nbsp;
                                        <input type="button" class="buttonfltbks" id="btnHideAgency" name="btn_edit" value="Agency Hide" onclick="HideAgency();" style="margin-top: -61px" />&nbsp;
                                        <asp:Button ID="Button1" runat="server" Text="ExportToExcel" BackColor="#004b91"
                                            Font-Bold="False" ForeColor="White" CausesValidation="False" OnClientClick="return hideagencydetail();"
                                            Visible="false" />
                                </span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="social-link" id="ExportToPdfFun" title="Download Ticket"></div>
                </li>
            </ul>

        </div>

        <div id="divtkt" runat="server" style="margin: 5px auto; width: 97%; background-color: #FFFFFF; padding: 5px;">
            <div class="large-12 medium-12 small-12">

                <div id="divprint" runat="server" style="margin: 5px auto; border: 1px #eee solid; width: 98%; background-color: #FFFFFF; padding: 5px;">
                    <div id="div_mail" runat="server">
                        <div style="clear: both;"></div>

                        <asp:Label ID="LabelTkt" runat="server"></asp:Label>

                        <div style="clear: both;"></div>

                    </div>
                </div>

                <%--<div id="divprint1" runat="server" style="margin: 5px auto; border: 1px #20313f solid; width: 90%; background-color: #FFFFFF; padding: 5px; display: none;">
                    <div id="div_mail">
                        <asp:Label ID="LabelTkt" runat="server"></asp:Label>
                    </div>
                </div>--%>
            </div>
        </div>
        <div id="Div_Main" runat="server">
            <div style="margin: 5px auto; width: 96%; background-color: #FFFFFF; padding: 10px;">
                <div class="large-12 medium-12 small-12" style="padding: 10px; background-color: #eeeeee;">
                    <div style="font-family: arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color: #004b91; padding-left: 10px; padding-top: 5px; padding-bottom: 5px;"
                        id="td_showaddchage">
                        <input type="button" class="buttonfltbk" id="btn_addcharge" name="btn_addcharge" value="ServiceCharge(+)" style="width: 135px; height: 30px"
                            onclick="showcharge();" />&nbsp;&nbsp;<br />
                        <div class="clear"></div>
                        <br />
                        <asp:Label ID="TaxNew" runat="server" Text=""></asp:Label>

                        &nbsp; <%--<span style="font-family: arial, Helvetica, sans-serif; padding: 8px; font-size: 12px; color: #FF3300">
                            <b style="font-family: arial, Helvetica, sans-serif; font-size: 13px; color: #004b91;">Note:</b>&nbsp; We are not
                                        storing any data regarding additional charge into our database.</span>--%>
                    </div>

                    <div id="td_servicecharge" style="display: none" class="row">
                        <div class="large-2 medium-2 small-4 col-md-4">
                            Select Charge Type
                            <asp:DropDownList ID="ddl_srvtype" runat="server">
                                <asp:ListItem Value="">--Select--</asp:ListItem>
                                <asp:ListItem Value="TC">Transaction Charge</asp:ListItem>
                                <asp:ListItem Value="TAX">Tax</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="large-2 medium-2 small-4 col-md-4">
                            Charge Amount
                                                        <a onclick="hidecharge();" href="#">
                                                            <div style="font-size: 14px; float: right; color: #424242;">×</div>
                                                        </a>
                            <input type="text" id="txt_srvcharge" name="txt_srvcharge" style="width: 95%;" runat="server" onkeypress="return NumericOnly(event);" />
                        </div>
                        <div class="large-2 medium-2 small-12">
                        </div>
                        <div class="clear"></div>

                        <div class="large-2 medium-2 small-6 large-push-10 medium-push-10 small-push-10">
                            <input type="button" class="buttonfltbk" id="btn_edit" name="btn_edit" value="Add Charge" onclick="AdditionalCharge();" style="width: 135px; height: 30px" />
                        </div>
                        <div class="clear"></div>
                        <div>
                            <b>NOTE:</b>&nbsp; Charge amount should be per pax
                        </div>

                        <div>
                            <%--For TC --%>
                            <input type="hidden" id="hidtcadt" name="hidtcadt" />
                            <input type="hidden" id="hidtcchd" name="hidtcchd" />

                            <input type="hidden" id="hidtotadt" name="hidtotadt" />
                            <input type="hidden" id="hidtotchd" name="hidtotchd" />

                            <input type="hidden" id="hidgrandtot" name="hidgrandtot" />
                            <input type="hidden" id="hidfinaltot" name="hidfinaltot" />

                            <%--For Tax --%>
                            <input type="hidden" id="hidtaxadt" name="hidtaxadt" />
                            <input type="hidden" id="hidtaxchd" name="hidtaxchd" />

                            <input type="hidden" id="hidtaxtotadt" name="hidtaxtotadt" />
                            <input type="hidden" id="hidtaxtotchd" name="hidtaxtotchd" />

                            <input type="hidden" id="hidtaxgrandtot" name="hidtaxgrandtot" />
                            <input type="hidden" id="hidtaxfinaltot" name="hidtaxfinaltot" />

                            <input type="hidden" id="hedtotInfant" name="hedtotInfant" />
                            <input type="hidden" id="hedFinalTotal" name="hedFinalTotal" />
                            <input type="hidden" id="hedFinalTotaltax" name="hedFinalTotaltax" />

                            <%--Pax Wise--%>

                            <input type="hidden" id="hidperpaxtc" name="hidperpaxtc" />
                            <input type="hidden" id="hidperpaxTCtot" name="hidperpaxTCtot" />
                            <input type="hidden" id="hidperpaxgrandTCtot" name="hidperpaxgrandtot" />

                            <input type="hidden" id="hidperpaxtax" name="hidperpaxtax" />
                            <input type="hidden" id="hidperpaxTaxtot" name="hidperpaxTaxtot" />
                            <input type="hidden" id="hidperpaxgrandTaxtot" name="hidperpaxgrandtot" />


                        </div>
                    </div>
                </div>

                <div>
                    <input type="hidden" id="Hidden1" runat="server" name="Hidden1" />
                </div>
            </div>

            <div style="margin: 10px auto; border: 1px #20313f solid; width: 90%; background-color: #FFFFFF; padding: 0px;" id="div2" runat="server">
                <%--<table width="100%" border="0" cellspacing="2" cellpadding="2" bgcolor="#20313f" style="height: 80px"
                    align="center">
                    <tr>
                        <td colspan="2" style="color: #ffffff; font-size: 12px;">
                            <strong style="padding-left: 10px">Send E-Mail:</strong>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #ffffff; font-size: 12px; padding-left: 15px;" valign="top">Email-ID :
                    <asp:TextBox ID="txt_email" Width="350" Height="35" runat="server" CssClass="textboxflight"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txt_email"
                                ErrorMessage="*" ForeColor="#990000" Display="Dynamic">*</asp:RequiredFieldValidator>
                            <br />
                            <div style="text-align: center; color: #EC2F2F">
                                <asp:RegularExpressionValidator ID="valRegEx" runat="server" ControlToValidate="txt_email"
                                    ValidationExpression=".*@.*\..*" ErrorMessage="*Invalid E-Mail ID." Display="dynamic">*Invalid E-Mail ID.</asp:RegularExpressionValidator>
                            </div>
                        </td>
                        <td style="text-align: left; padding-top: 18px;" width="40%" valign="middle">
                            <asp:Button ID="btn" runat="server" OnClientClick="return emailvalidate();" CssClass="buttonfltbk" Text="Send" Style="width: 135px; height: 30px"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="color: red; font-size: 12px; padding-left: 15px;">
                            <asp:Label ID="mailmsg" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>--%>
            </div>
        </div>
        <asp:HiddenField ID="HdnOrderId" runat="server" />
        <asp:HiddenField ID="HdnTrnsId" runat="server" />
        <asp:HiddenField ID="HidDivFare" runat="server" />




        <%-- <div style="position: fixed;display:none; top: 0%;  width: 100%; height: 100%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000;">
                    Proccessing......<br />
                    <br />
                    <img alt="loading" style="margin-top:200px" src="<%= ResolveUrl("~/images/wait.gif")%>" />
                    <br />
                </div>--%>

        <script src="../Scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function emailvalidate() {
                ;
                if ($("#txt_email").val() == "" || $("#txt_email").val() == " ") {
                    alert("Please Provide valid emailID.")
                    return false;
                }
                var emailPat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                var emailid = document.getElementById("txt_email").value;
                var matchArray = emailid.match(emailPat);
                if (matchArray == null) {
                    alert("Your email address seems incorrect. Please try again.");
                    document.getElementById("txt_email").focus();
                    return false;
                }
            }
        </script>

        <script language="javascript" type="text/javascript">
            function NumericOnly(event) {

                if (event.which != 46 && event.which != 45 && event.which != 46 &&
                    !(event.which >= 48 && event.which <= 57)) {
                    return false;
                }

                //var charCode = (event.keyCode ? event.keyCode : event.which);
                //if (charCode > 31 && (charCode < 48 || charCode > 57))
                //    return false;
                //return true;

            }
            function showcharge() {
                $("#td_servicecharge").show();
                $("#td_showaddchage").hide();
            }
            function hidecharge() {
                debugger;
                $("#td_servicecharge").hide();
                $("#td_showaddchage").show();
                if ($("#HidDivFare").val() == "hide") {
                    HideFare();
                    //ShowFare()
                    //$("#HidDivFare").val("hide");
                }
                else {
                    ShowFare();
                }

            }
            function AdditionalCharge() {
                if ($("#ddl_srvtype").val() == "") {
                    alert('Please select charge type');
                    $("#ddl_srvtype").focus();
                    return false;
                }
                if ($("#txt_srvcharge").val() == "") {
                    alert('Please fill charge amount');
                    $("#txt_srvcharge").focus();
                    return false;
                }
                //Get Query string 
                var collection = {}; var k = 0;
                var pgUrl = window.location.search.substring(1);
                var qarray = pgUrl.split('&');
                for (var i = 0; i <= qarray.length - 1; i++) {
                    var splt = qarray[i].split('=');
                    if (splt.length > 0) {
                        for (var j = 0; j < splt.length - 1; j++) {
                            collection[k] = splt[j + 1];
                        }
                        k += 1;
                    }
                }
                if (collection[1] == "") {
                    //Claculation for whole order

                    var SrvCharge = $("#txt_srvcharge").val();
                    var SrvType = $("#ddl_srvtype").val();
                    var adtcnt = $("#td_adtcnt")[0].innerHTML;
                    var td_taxadt = $("#td_taxadt").html();

                    var chdcnt = 0;
                    var tcadt = 0, tcadttot = 0, tcchd = 0, tcchdtot = 0;
                    var taxadt = 0, taxadttot = 0, taxchd = 0, taxchdtot = 0, TotalInfant = 0, FinalTotal = 0;;

                    if ($('#td_chdcnt').length > 0) {
                        chdcnt = $("#td_chdcnt")[0].innerHTML;
                    }
                    if (SrvType == "TC") {

                        if ($("#hedFinalTotal").val() != "") {
                            $("#td_grandtot")[0].innerHTML = parseInt($("#hedFinalTotal").val());
                        }
                        if ($("#hedFinalTotal").val() == "") {

                            FinalTotal = $("#hedFinalTotal").val($("#td_grandtot")[0].innerHTML).val();
                        }
                        else {
                            FinalTotal = $("#hedFinalTotal").val();
                        }
                        if (adtcnt > 0) {
                            //Checking hidden field for tax
                            if ($("#hidtcadt").val() != "") {
                                $("#td_tcadt")[0].innerHTML = parseInt($("#hidtcadt").val());
                            }
                            if ($("#hidtotadt").val() != "") {
                                $("#td_adttot")[0].innerHTML = parseInt($("#hidtotadt").val());
                            }
                            if ($("#hidtcadt").val() == "") {
                                tcadt = $("#hidtcadt").val($("#td_tcadt")[0].innerHTML).val();
                            }
                            else {
                                tcadt = $("#hidtcadt").val();
                            }

                            if ($("#hidtotadt").val() == "") {
                                tcadttot = $("#hidtotadt").val($("#td_adttot")[0].innerHTML).val();
                            }
                            else {
                                tcadttot = $("#hidtotadt").val();
                            }
                            $("#td_tcadt")[0].innerHTML = parseInt(tcadt) + (parseInt(SrvCharge) * parseInt(adtcnt));
                            $("#td_adttot")[0].innerHTML = parseInt(tcadttot) + (parseInt(SrvCharge) * parseInt(adtcnt));

                        }
                        if (chdcnt > 0) {
                            //For CHD TC

                            if ($("#hidtcchd").val() != "") {
                                $("#td_tcchd")[0].innerHTML = parseInt($("#hidtcchd").val());
                            }

                            if ($("#hidtotchd").val() != "") {
                                $("#td_chdtot")[0].innerHTML = parseInt($("#hidtotchd").val());
                            }

                            if ($("#hidtcchd").val() == "") {
                                tcchd = $("#hidtcchd").val($("#td_tcchd")[0].innerHTML).val();
                            }

                            else {
                                tcchd = $("#hidtcchd").val();
                            }

                            if ($("#hidtotchd").val() == "") {
                                tcchdtot = $("#hidtotchd").val($("#td_chdtot")[0].innerHTML).val();
                            }
                            else {
                                tcchdtot = $("#hidtotchd").val();
                            }

                            $("#td_tcchd")[0].innerHTML = parseInt(tcchd) + (parseInt(SrvCharge) * parseInt(chdcnt));
                            $("#td_chdtot")[0].innerHTML = parseInt(tcchdtot) + (parseInt(SrvCharge) * parseInt(chdcnt));
                        }

                        $("#td_grandtot")[0].innerHTML = parseInt(FinalTotal) + (parseInt(SrvCharge) * parseInt(adtcnt)) + (parseInt(SrvCharge) * parseInt(chdcnt));
                        $("#lbltransfee")[0].innerHTML = (parseInt(SrvCharge) * parseInt(adtcnt)) + (parseInt(SrvCharge) * parseInt(chdcnt));
                        $("#trtransfee").show();
                    }

                    if (SrvType == "TAX") {
                        if ($("#hedFinalTotaltax").val() != "") {
                            $("#td_grandtot")[0].innerHTML = parseInt($("#hedFinalTotaltax").val());
                        }
                        if ($("#hedFinalTotaltax").val() == "") {

                            FinalTotal = $("#hedFinalTotaltax").val($("#td_grandtot")[0].innerHTML).val();
                        }
                        else {
                            FinalTotal = $("#hedFinalTotaltax").val();
                        }
                        if (adtcnt > 0) {
                            if ($("#hidtaxadt").val() != "") {
                                $("#td_taxadt")[0].innerHTML = parseInt($("#hidtaxadt").val());
                            }
                            if ($("#hidtaxtotadt").val() != "") {
                                $("#td_adttot")[0].innerHTML = parseInt($("#hidtaxtotadt").val());
                            }
                            //For Adult TAX
                            if ($("#hidtaxadt").val() == "") {
                                taxadt = $("#hidtaxadt").val($("#td_taxadt")[0].innerHTML).val();
                            }
                            else {
                                taxadt = $("#hidtaxadt").val();
                            }
                            if ($("#hidtaxtotadt").val() == "") {
                                taxadttot = $("#hidtaxtotadt").val($("#td_adttot")[0].innerHTML).val();
                            }
                            else {
                                taxadttot = $("#hidtaxtotadt").val();
                            }
                            $("#td_taxadt")[0].innerHTML = parseInt(taxadt) + (parseInt(SrvCharge) * parseInt(adtcnt));
                            $("#td_adttot")[0].innerHTML = parseInt(taxadttot) + (parseInt(SrvCharge) * parseInt(adtcnt));

                        }
                        if (chdcnt > 0) {
                            if ($("#hidtaxchd").val() != "") {
                                $("#td_taxchd")[0].innerHTML = parseInt($("#hidtaxchd").val());
                            }

                            if ($("#hidtaxtotchd").val() != "") {
                                $("#td_chdtot")[0].innerHTML = parseInt($("#hidtaxtotchd").val());
                            }

                            if ($("#hidtaxgrandtot").val() != "") {
                                $("#td_grandtot")[0].innerHTML = parseInt($("#hidtaxgrandtot").val());
                            }

                            //For Child TAX
                            if ($("#hidtaxchd").val() == "") {
                                taxchd = $("#hidtaxchd").val($("#td_taxchd")[0].innerHTML).val();
                            }

                            else {
                                taxchd = $("#hidtaxchd").val();
                            }

                            if ($("#hidtaxtotchd").val() == "") {
                                taxchdtot = $("#hidtaxtotchd").val($("#td_chdtot")[0].innerHTML).val();
                            }

                            else {
                                taxchdtot = $("#hidtaxtotchd").val();
                            }

                            $("#td_taxchd")[0].innerHTML = parseInt(taxchd) + (parseInt(SrvCharge) * parseInt(chdcnt));
                            $("#td_chdtot")[0].innerHTML = parseInt(taxchdtot) + (parseInt(SrvCharge) * parseInt(chdcnt));
                        }
                                                                     
                        $(".taxclass")[0].innerHTML = parseInt(td_taxadt) + (parseInt(SrvCharge) * parseInt(adtcnt)) + (parseInt(SrvCharge) * parseInt(chdcnt));
                        $("#td_grandtot")[0].innerHTML = parseInt(FinalTotal) + (parseInt(SrvCharge) * parseInt(adtcnt)) + (parseInt(SrvCharge) * parseInt(chdcnt));
                        $("#lbltransfee")[0].innerHTML = (parseInt(SrvCharge) * parseInt(adtcnt)) + (parseInt(SrvCharge) * parseInt(chdcnt));
                    }
                    //$("#td_tcadt").focus();
                    UpdateCharges();
                    alert('Fare summary changed sucessfully.');
                }
                else {
                    //Calculation by pax id
                    var SrvCharge = $("#txt_srvcharge").val();
                    var SrvType = $("#ddl_srvtype").val();
                    var tcperpax = 0, tcpaxTotal = 0, perpaxgrandtot = 0;
                    var taxperpax = 0;
                    var paxtype = $("#td_perpaxtype")[0].innerHTML;
                    if (paxtype == "INF") {
                        alert('Fare will not change for Infant');
                        return false;
                    }
                    if (SrvType == "TC" && paxtype != "INF") {

                        if ($("#hidperpaxtc").val() != "") {
                            $("#td_perpaxtc")[0].innerHTML = parseInt($("#hidperpaxtc").val());
                        }
                        if ($("#hidperpaxTCtot").val() != "") {
                            $("#td_totalfare")[0].innerHTML = parseInt($("#hidperpaxTCtot").val());
                        }
                        if ($("#hidperpaxgrandTCtot").val() != "") {
                            $("#td_grandtot")[0].innerHTML = parseInt($("#hidperpaxgrandTCtot").val());
                        }
                        if ($("#hidperpaxtc").val() == "") {
                            tcperpax = $("#hidperpaxtc").val($("#td_perpaxtc")[0].innerHTML).val();
                        }
                        else {
                            tcperpax = $("#hidperpaxtc").val();
                        }
                        if ($("#hidperpaxTCtot").val() == "") {
                            tcpaxTotal = $("#hidperpaxTCtot").val($("#td_totalfare")[0].innerHTML).val();
                        }
                        else {
                            tcpaxTotal = $("#hidperpaxTCtot").val();
                        }
                        if ($("#hidperpaxgrandTCtot").val() == "") {
                            perpaxgrandtot = $("#hidperpaxgrandTCtot").val($("#td_grandtot")[0].innerHTML).val();
                        }
                        else {
                            perpaxgrandtot = $("#hidperpaxgrandTCtot").val();
                        }
                        $("#td_perpaxtc")[0].innerHTML = parseInt(tcperpax) + (parseInt(SrvCharge));
                        $("#td_totalfare")[0].innerHTML = parseInt(tcpaxTotal) + (parseInt(SrvCharge));
                        $("#td_grandtot")[0].innerHTML = parseInt(perpaxgrandtot) + (parseInt(SrvCharge));
                    }
                    if (SrvType == "TAX" && paxtype != "INF") {

                        if ($("#hidperpaxtax").val() != "") {
                            $("#td_perpaxtax")[0].innerHTML = parseInt($("#hidperpaxtax").val());
                        }
                        if ($("#hidperpaxTaxtot").val() != "") {
                            $("#td_totalfare")[0].innerHTML = parseInt($("#hidperpaxTaxtot").val());
                        }
                        if ($("#hidperpaxgrandTaxtot").val() != "") {
                            $("#td_grandtot")[0].innerHTML = parseInt($("#hidperpaxgrandTaxtot").val());
                        }
                        if ($("#hidperpaxtax").val() == "") {
                            taxperpax = $("#hidperpaxtax").val($("#td_perpaxtax")[0].innerHTML).val();
                        }
                        else {
                            taxperpax = $("#hidperpaxtax").val();
                        }
                        if ($("#hidperpaxTaxtot").val() == "") {
                            tcpaxTotal = $("#hidperpaxTaxtot").val($("#td_totalfare")[0].innerHTML).val();
                        }
                        else {
                            tcpaxTotal = $("#hidperpaxTaxtot").val();
                        }
                        if ($("#hidperpaxgrandTaxtot").val() == "") {
                            perpaxgrandtot = $("#hidperpaxgrandTaxtot").val($("#td_grandtot")[0].innerHTML).val();
                        }
                        else {
                            perpaxgrandtot = $("#hidperpaxgrandTaxtot").val();
                        }
                        $("#td_perpaxtax")[0].innerHTML = parseInt(taxperpax) + (parseInt(SrvCharge));
                        $("#td_totalfare")[0].innerHTML = parseInt(tcpaxTotal) + (parseInt(SrvCharge));
                        $("#td_grandtot")[0].innerHTML = parseInt(perpaxgrandtot) + (parseInt(SrvCharge));
                    }
                    UpdateCharges();
                    alert('Fare summary changed sucessfully.');
                    // $("#td_perpaxtax").focus();
                }
                $("#Hidden1").val($("#div_mail")[0].innerHTML);
            }
            function UpdateCharges() {
                $.ajax({
                    type: "POST",
                    //url: "PnrSummaryIntl.aspx/GetCurrentTime",
                    //data: '{name: "' + $("#txt_srvcharge").val() + '" }',
                    url: "PnrSummaryIntl.aspx/UpdateCharges",
                    data: '{OrderId: "' + $("#HdnOrderId").val() + '",Amount: "' + $("#txt_srvcharge").val() + '",ChargeType: "' + $("#ddl_srvtype").val() + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnSuccess,
                    failure: function (response) {
                        //alert(response.d);
                    }
                });
            }
            function OnSuccess(response) {
                //alert(response.d);        
            }

            function ShowAgency() {
                $("#trLogoWithAgencySection").show();
                $("#btnShowAgency").hide();
                $("#btnHideAgency").show();
            }
            function HideAgency() {
                $("#trLogoWithAgencySection").hide();
                $("#btnShowAgency").show();
                $("#btnHideAgency").hide();
            }

            function ShowFare() {
                $("#disfareinfoheader").show();
                $("#disfareinfo").show();
                $("#hdpaxinfo").hide();
                $("#TR_FareInformation1").show();
                $("#TR_FareInformation2").show();
                $("#TR_FareInformation3").show();
                $("#TR_FareInformation4").show();
                $("#TR_FareInformation5").show();
                $("#TR_FareInformation6").show();
                $("#TR_FareInformation7").show();
                $("#TR_FareInformation8").show();
                $("#TR_FareInformation9").show();
                $("#TR_FareInformation10").show();
                $("#TR_FareInformation11").show();
                //$("#LabelTkt").show();
                $("#BtnHideFare").show();
                $("#BtnShowFare").hide();

                $("#HidDivFare").val("");

            }
            function HideFare() {
                $("#BtnHideFare").hide();
                $("#BtnShowFare").show();
                $("#disfareinfoheader").hide();
                $("#disfareinfo").hide();
                $("#hdpaxinfo").show();
                $("#HidDivFare").val("hide");
                $("#TR_FareInformation1").hide();
                $("#TR_FareInformation2").hide();
                $("#TR_FareInformation3").hide();
                $("#TR_FareInformation4").hide();
                $("#TR_FareInformation5").hide();
                $("#TR_FareInformation6").hide();
                $("#TR_FareInformation7").hide();
                $("#TR_FareInformation8").hide();
                $("#TR_FareInformation9").hide();
                $("#TR_FareInformation10").hide();
                $("#TR_FareInformation11").hide();
                //$("#	GRAND TOTAL").hide();
                //$("#td_showaddchage").show();
            }
        </script>
        <uc1:PnrSummaryIntl runat="server" ID="PnrSummaryIntl" />
    </form>
</body>
</html>
