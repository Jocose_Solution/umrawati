﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="true" CodeFile="recharge-trans-report.aspx.cs" Inherits="Report_Dmt1_recharge_trans_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/Styles/jAlertCss.css")%>" rel="stylesheet" />
    <script src="<%=ResolveUrl("~/Scripts/ReissueRefund.js?v=1")%>" type="text/javascript"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/PopupScript.js?V=1")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/ScriptsPNRCancellation/CancelPNR.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/jquery.blockUI.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/jquery.blockUI.js")%>"></script>
    <link type="text/css" href="<%=ResolveUrl("~/Content/Popup.css")%>" rel="stylesheet" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl(" ~ /Scripts/gridview-readonly-script.js")%>"></script>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }

        .tooltip1 {
            position: relative;
        }

        .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: black;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            /* Position the tooltip */
            position: absolute;
            z-index: 1;
        }

        #tooltip {
            z-index: 9999;
            position: absolute;
            top: 200px;
            float: right;
            padding: 5px;
            right: 280px;
            border: 2px solid #04034f;
            background-color: #fff;
            width: auto;
            min-width: 300px;
        }

        .table .table {
            background-color: #fff;
            border: 1px solid #ccc;
        }

        .tooltip1:hover .tooltiptext {
            visibility: visible;
        }

        .popupnew2 {
            position: absolute;
            top: 10px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .hovercolor {
            font-weight: bold;
            color: #004b91;
            font-size: 11px;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: auto !important;
            overflow-y: auto !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }
    </style>
    <style>
        .pagination {
            margin: 0;
            /*margin-bottom: 30px;*/
        }

            .pagination > span > a, .pagination > span > span {
                background-color: #fff;
                border: 1px solid #ddd;
                display: inline;
                float: left;
                line-height: 1.42857;
                margin-left: -1px;
                padding: 6px 12px;
                position: relative;
                text-decoration: none;
            }

            .pagination > span > a {
                color: #E7302A !important;
            }

            .pagination > span > span {
                color: #fff !important;
            }

                .pagination > span > a:first-child, .pagination > span > span:first-child {
                    border-bottom-left-radius: 4px;
                    border-top-left-radius: 4px;
                    margin-left: 0;
                    color: #E7302A !important;
                }

                .pagination > span > a:last-child, .pagination > span > span:last-child {
                    border-bottom-right-radius: 4px;
                    border-top-right-radius: 4px;
                    color: #E7302A !important;
                }

            .pagination > span > span {
                background-color: #E7302A !important;
                color: #FFF;
                cursor: default;
                z-index: 2;
            }

            .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
                z-index: 3;
                color: #fff;
                cursor: default;
                background-color: #00c2ff !important;
                border-color: #00c2ff !important;
                cursor: pointer;
            }

            .pagination > li > a, .pagination > li > span {
                cursor: pointer;
            }
    </style>

    <div class="">
        <div class="card-header">
            <div class="col-md-12">
                <h3 style="text-align: center; color: orange">Richarge-Bill-Trans Report</h3>
                <hr style="height: 3px; background: orange" />
            </div>
        </div>

        <div class="card-body">
            <div class="col-md-12">
                <div class="row form-group">
                    <div class="col-md-3">
                        <label>From</label>
                        <input type="text" name="From" id="From" placeholder="Select Date" class="form-control" readonly="readonly" />
                    </div>
                    <div class="col-md-3">
                        <label>To</label>
                        <input type="text" name="To" placeholder="Select Date" id="To" class="form-control" readonly="readonly" />
                    </div>
                    <div class="col-md-3">
                        <label>Client Ref. Id</label>
                        <asp:TextBox ID="txt_Trackid" placeholder="Enter trackid" class="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <label>Transaction Type</label>
                        <asp:DropDownList class="form-control" ID="ddltransType" runat="server">
                            <asp:ListItem Value="">All</asp:ListItem>
                            <asp:ListItem Value="mobile">Mobile</asp:ListItem>
                            <asp:ListItem Value="dth">DTH</asp:ListItem>
                            <asp:ListItem Value="electricity">Electricity</asp:ListItem>
                            <asp:ListItem Value="landline">Landline</asp:ListItem>
                            <asp:ListItem Value="insurance">Insurance</asp:ListItem>
                            <asp:ListItem Value="gas">Gas</asp:ListItem>
                            <asp:ListItem Value="broadband">Broadband</asp:ListItem>
                            <asp:ListItem Value="water">Water</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label>Status</label>
                        <asp:DropDownList class="form-control" ID="ddlstatus" runat="server">
                            <asp:ListItem Value="">All</asp:ListItem>
                            <asp:ListItem Value="successful">Success</asp:ListItem>
                            <asp:ListItem Value="under process">Under Process</asp:ListItem>
                            <asp:ListItem Value="failed">Failed</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <br />
                        <asp:Button ID="btn_result" runat="server" class="btn btn-danger form-control maincss" Text="Search Result" OnClick="btn_result_Click" />
                    </div>
                    <div class="col-sm-3">
                        <br />
                        <asp:Button ID="btn_export" runat="server" class="btn btn-danger form-control" Text="Export" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12 form-group">
                        <div class="table-responsive text-nowrap">
                            <table class="rtable" data-toggle="table" style="width: 100%!important">
                                <asp:ListView ID="lstTransaction" runat="server" OnPagePropertiesChanged="lstTransaction_PagePropertiesChanged">
                                    <LayoutTemplate>
                                        <tr>
                                            <th>TxnDate</th>
                                            <th>OrderID</th>
                                            <th>Client Ref. ID </th>
                                            <th>Number</th>
                                            <th>Amount</th>
                                            <th>Service</th>
                                            <th>Operator Name</th>
                                            <th>Area Name</th>
                                            <th>Status</th>
                                        </tr>
                                        <div runat="server" id="ItemPlaceholder"></div>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("transDate") %></td>
                                            <td><%# Eval("TransactionId") %></td>
                                            <td><%# Eval("ClientRefId") %></td>
                                            <td><%# Eval("Number") %></td>
                                            <td><%# Eval("Amount") %></td>
                                            <td><%# Eval("ServiceType") %></td>
                                            <td><%# Eval("Operator") %></td>
                                            <td><%# Eval("CircleName") %></td>
                                            <td><%# Eval("status").ToString().ToLower().Contains("successful")?"Success":(Eval("status").ToString().ToLower().Contains("failed")?"Failed":(Eval("status").ToString().ToLower().Contains("under process")?"Under Process":"- - -")) %></td>
                                        </tr>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                        <table class="rtable" data-toggle="table" style="width: 100%!important">
                                            <tr>
                                                <th>TxnDate</th>
                                                <th>OrderID</th>
                                                <th>Client Ref. ID </th>
                                                <th>Number</th>
                                                <th>Amount</th>
                                                <th>Service</th>
                                                <th>Operator Name</th>
                                                <th>Area Name</th>
                                                <th>Status</th>
                                            </tr>
                                            <tr>
                                                <td colspan="9" style="color: red; text-align: center;">NO RECORD FOUND !</td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                </asp:ListView>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <ul class="pagination">
                            <asp:DataPager runat="server" ID="PagingButtom" PagedControlID="lstTransaction" PageSize="10">
                                <Fields>
                                    <asp:NextPreviousPagerField FirstPageText="First" PreviousPageText="&laquo;" ShowFirstPageButton="false" ShowNextPageButton="false" ShowPreviousPageButton="true" RenderDisabledButtonsAsLabels="false" />
                                    <asp:NumericPagerField ButtonCount="5" />
                                    <asp:NextPreviousPagerField LastPageText="Last" NextPageText="&raquo;" ShowLastPageButton="false" ShowNextPageButton="true" ShowPreviousPageButton="false" RenderDisabledButtonsAsLabels="false" />
                                </Fields>
                            </asp:DataPager>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            InitializeToolTip();
        });
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/alert.js")%>"></script>
</asp:Content>

