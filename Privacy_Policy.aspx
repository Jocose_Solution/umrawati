﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="Privacy_Policy.aspx.cs" Inherits="Privacy_Policy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="container"><h1 class="center">Terms &amp; Conditions</h1>


<h2>Introduction and Terms of Use</h2>


<p>The terms and conditions contained hereinafter (" <strong>Terms of Use</strong>") shall apply to the use of the website <a href="https://www.FastGoCash.com">www.FastGoCash.com</a> and any other linked pages, products, software(s), API keys, features, content or application services (including but without limitation to any mobile application services) in connection therewith, offered from time to time by FastGoCash Private Limited (" <strong>FastGoCash</strong>" or " <strong>we</strong>" or " <strong>our</strong>" or " <strong>us</strong>") (collectively, " <strong>Website</strong>").</p>
<p>Any person logging on to or using the Website (even when such person does not avail of any services provided in the Website (" <strong>Services</strong>")) (hereinafter referred to as a " <strong>User</strong>", " <strong>seller</strong>", " <strong>you</strong>" or " <strong>Client</strong>") shall be presumed to have read these Terms of Use (which includes the Privacy Policy, separately provided on the Website) and unconditionally accepted the terms and conditions set out herein. These Terms of Use constitute a binding and enforceable agreement between the User and FastGoCash. These Terms of Use do not alter in any way the terms or conditions of any other written agreement you may have with FastGoCash for other services.</p>
<p>Please read the terms set out hereunder carefully before agreeing to the same. If you do not agree to these Terms of Use (including any referenced policies or guidelines), please immediately terminate your use of the Website. You can accept the Terms of Use by:</p>


<p>Clicking to accept or agree to the Terms of Use, where this option is made available to you by FastGoCash in the User interface for any particular Service; or</p>


<p>Accessing, testing or actually using the Services. In this case, you understand and agree that FastGoCash will treat your use of the Services as acceptance of the Terms of Use from that point onwards.</p>


<p>For the purpose of these Terms of Use, wherever the context so requires, the term "User" shall mean and include any natural or legal person who has agreed to these Terms of Use on behalf of itself or any other legal entity.</p>
<p>It is clarified that the Privacy Policy (that is provided separately), form an integral part of these Terms of Use and should be read contemporaneously with the Terms of Use. Illegality or unenforceability of one or more provisions of these Terms of Use shall not affect the legality and enforceability of the other terms of the Terms of Use. For avoidance of doubt, if any of the provisions becomes void or unenforceable, the rest of the provisions of these Terms of Use shall be binding upon the User.</p>
<p>The Terms of Use may be revised or altered by us at our sole discretion at any time without any prior intimation to the User. The latest Terms of Use will be posted here. Any such changes by FastGoCash will be effective immediately. By continuing to use this Website or to access the Services / usage of our Services after changes are made, you agree to be bound by the revised/ amended Terms of Use and such amendments shall supersede all other terms of use previously accepted by the User. You are solely responsible for understanding and complying with all applicable laws of your specific jurisdiction that may be applicable to you in connection with your business and use of our Services.</p>


<h2>Provision of the Services being offered by FastGoCash</h2>


<p>Subject to these Terms of Use, FastGoCash offers the Services set forth in Schedule I herein.</p>
<p>Schedule I also contains descriptions and process flows of all Services that are offered to Users. These descriptions and process flows are set out in Part A to Part G in Schedule I.</p>
<p>FastGoCash is constantly evolving in order to provide the best possible experience and information to its Users. You acknowledge and agree that the form and nature of the Services which FastGoCash provides may change from time to time without any prior notice to you.</p>
<p>As part of this continuing process, you acknowledge and agree that FastGoCash may stop (permanently or temporarily) providing the Services (or any features within the Services) to you or to Users generally at FastGoCash's sole discretion, without any prior notice. You may stop using the Services at any point of time. You do not need to specifically inform FastGoCash when you stop using the Services.</p>
<p>You acknowledge and agree that if FastGoCash disables access to your account, you may be prevented from accessing the Services, your account details or any files or other content which is contained in your account, and FastGoCash shall intimate you regarding the same.</p>
<p>FastGoCash reserves the right to delete your User information stored in your account including but not limited to all or any personal information or any sensitive personal data or information (" <strong>SPDI</strong>") stored in your User account. Alternately, a User may notify us if they do not wish that we retain or use the personal information or SPDI by contacting the Grievance Officer/Nodal Officer (as provided below). However, in such a case, we may not be able to provide you some or all of our Services.</p>
<p>You acknowledge and agree that while FastGoCash may not currently have set a fixed upper limit on the number of transmissions you may send or receive through the Services, such fixed upper limits may be set by FastGoCash at any time, solely at FastGoCash's discretion.</p>
<p>By using our Services you agree that FastGoCash disclaims any liability or authenticity of any information that may have become outdated since the last time that particular piece of information was updated. FastGoCash reserves the right to make changes and corrections to any part of the content of this Website at any time without any prior notice to you. Unless stated otherwise, all pictures and information contained on this Website are believed to be in the public domain as either promotional materials, publicity photos, photoshoot rejects or press media stock. Please contact the Grievance Officer/Nodal Officer by an e-mail if you are the copyright owner of any content on this Website and you think the use of the above material violates the terms of the applicable Copyright law in any manner. In your request, please indicate the exact URL of the webpage to enable us to locate the same. We will endeavour to address your concerns and take necessary steps, if required. Please note that all images displayed on the Website have been digitised by FastGoCash. No other party is authorised to reproduce or republish these digital versions in any format whatsoever without the prior written permission of FastGoCash.</p>
<p>FastGoCash acknowledges and represents that it is and shall remain compliant with the applicable information and data security mandates prescribed under the RBI Guidelines on Regulation of Payment Aggregators and Payment Gateways, dated March 17, 2020 (" <strong>Guidelines</strong>"). Further, FastGoCash shall maintain compliance with PCI DSS Standard or other comparable industry standards governing physical/ logical security cardholder data across FastGoCash environment and ensure that the card holder's data is secured in accordance with the standards.</p>


<h2>Use of the Services by User</h2>


<p>In order to access certain Services, you may be required to open a User account with FastGoCash by providing information about yourself (such as identification or contact details) as part of the registration process (" <strong>Registration Data</strong>") for the Services, or as part of your continued use of the Services. You agree that any Registration Data you give to FastGoCash will always be accurate, correct, complete and up to date. If you provide any information that is untrue, inaccurate, incomplete, or not current or if we have reasonable grounds to suspect that such information is in violation of applicable law or not in accordance with the Terms of Use (whether wholly or in part), we reserve the right to reject your registration and/ or indefinitely suspend or terminate your User account and refuse to provide you access to the Website. Further, you agree to indemnify and keep us indemnified from and against all claims resulting from the use of any detail/ information/ Registration Data that you post and/ or supply to us. We shall be entitled to remove any such detail/ information/ Registration Data posted by you without any prior intimation to you.</p>
<p>Notwithstanding anything else contained in any other agreement involving you and FastGoCash and/ or any other third party, in order to ensure that we are not violating any right that you might have in your Registration Data, you hereby grant to us a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sub-licensable right to exercise the copyright, publicity, and database rights (but no other rights) that you have in the Registration Data, in any media now or in future known, with respect to your Registration Data solely to enable us to use such Registration Data that you have supplied to us.</p>
<p>Any amendment or rectification of your Registration Data in the User account can be carried out by accessing the "User account" section on the Website. You may choose to delete any or all of your User content/ information or even the User account at any time. Processing such deletion may take some time, but the same shall be done by FastGoCash. We may maintain backup of all User content for such time as may be required under applicable laws and for operational purposes of FastGoCash. You are solely responsible for maintaining the confidentiality of your account and password and for any activity that occurs in or through your account. We will not be liable to any person for any loss or damage which may arise as a result of any failure on your part to protect your login ID or password or any other credential pertaining to your account. You should take all necessary steps to ensure that the password is kept confidential and secure. In case you have any reason to believe that your password has become known to anyone else, or if the password is being, or is likely to be, used in an unauthorised manner, you should inform us immediately at <a href="mailto: support@FastGoCash.com"> support@FastGoCash.com</a>. In the event of any dispute between two or more parties as to ownership of any particular account with FastGoCash, you agree that FastGoCash shall be the sole arbitrator for such dispute and that FastGoCash's decision in this regard will be final and binding on you.</p>
<p>You understand and undertake that you shall be solely responsible for your Registration Data and User content and undertake to, neither by yourself nor by permitting any third party to host, display, upload, modify, publish, transmit, update or share any information that:</p>


<p>Belongs to another person and to which you do not have any right to;</p>


<p>Is grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic, pedophilic, seditious, libelous, invasive of another's privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatsoever;</p>


<p>Harms minors in any way;</p>


<p>Infringes any patent, trademark, copyright or other proprietary rights of any person or entity anywhere in the world;</p>


<p>Violates any law for the time being in force;</p>


<p>Deceives or misleads the addressee about the origin of such messages or communicates any information which is grossly offensive or menacing in nature;</p>


<p>Impersonates another person;</p>


<p>Contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource;</p>


<p>Threatens the unity, integrity, defense, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents investigation of any offence or is insulting to any other nation; or</p>


<p>Is illegal in any other way.</p>


<p>You agree and understand that FastGoCash reserves the right to remove and/or edit such detail/ information. If you come across any information as mentioned above on the Website, you are requested to immediately contact our Grievance Officer/Nodal Officer.</p>
<p>You agree to use the Services only for purposes that are permitted by (a) these Terms of Use and (b) any applicable law, regulation or generally accepted practices or guidelines in the relevant jurisdictions.</p>
<p>You agree to use the data owned by FastGoCash (as available on the Website or through any other means like API(s) etc.) only for personal purposes and not for any commercial use unless agreed to by FastGoCash in writing.</p>
<p>You agree not to access (or attempt to access) any of the Services by any means other than through the interface that is provided by FastGoCash, unless you have been specifically allowed to do so in a separate agreement with FastGoCash. You specifically agree not to access (or attempt to access) any of the Services through any automated means (including use of scripts or web crawlers) and shall ensure that you comply with the instructions set out in any robots.txt file present on the Services.</p>
<p>You agree that you will not engage in any activity that interferes with or disrupts the Services (or the servers and networks which are connected to the Services) on this Website.</p>
<p>Unless you have been specifically permitted to do so in a separate agreement with FastGoCash, you agree that you will not reproduce, duplicate, copy, sell, trade or resell the Services for any purpose.</p>
<p>You agree that you are solely responsible for (and that FastGoCash has no responsibility to you or to any third party for) any breach of your obligations under the Terms of Use and for the consequences (including any loss or damage which FastGoCash may suffer) of any such breach. You further agree to the use of your data by us in accordance with the Privacy Policy.</p>
<p>FastGoCash may share any Content (<em>defined hereinafter</em>) generated by the User or their Registration Data with governmental and regulatory agencies who are lawfully authorised for investigative, protective and cyber security activities. Such information may be transferred for the purposes of verification of identity, or for prevention, detection, investigation, prosecution pertaining to cyber security incidents and punishment of offences under any law for the time being in force.</p>
<p>If you have opted for use of FastGoCash's 'subscriptions' product by virtue of which your customers have set up a standing instruction (" <strong>Recurring Payment Instruction</strong>") to charge his/ her chosen payment method (such as credit card, debit card or bank account) as per the billing cycle communicated by you to FastGoCash, then you consent that the relevant amount will be charged to such payment method as per the billing cycle communicated to FastGoCash. You agree that FastGoCash shall continue to charge the relevant amount to the relevant customer's chosen payment method as per such billing cycle until you or the customer terminates the Recurring Payment Instruction.</p>
<p>You further agree that if the customer revokes his/ her consent to the Recurring Payment Instruction then you shall inform us of the revocation in writing for us to cease processing the Recurring Payment Instruction forthwith upon the revocation but no later than 7 (seven) days prior to the next instance of charge to the customer. If such intimation of revocation of Recurring Payment Instruction is made less than 7 (seven) days prior to the next instance of charge to the customer or is not made at all, FastGoCash shall not be liable for any charge applied to the customer for that month pursuant to the revocation or not be liable at all, as applicable. You agree to make good any losses suffered by us on account of demands or claims from customers arising as a consequence of your failure in notifying us about a customer's revocation of the Recurring Payment Instruction.</p>


<h2>Eligibility</h2>


<p>Any person who is above eighteen (18) years of age and competent to contract under the applicable laws is eligible to access or visit the Website or avail the Services displayed therein. Your use or access of the Website shall be treated as your representation that you are competent to contract and if you are registering as a business entity, then you represent and warrant that you have the authority to bind such business entity to the Terms of Use. Without generality of the foregoing, use of the Website is available only to persons who can form a legally binding contract under the Indian Contract Act, 1872 and any amendments thereto.</p>
<p>The User represents and warrants that it will be financially responsible for all of User's usage (including the purchase of any Service) and access of the Website. The User shall also be responsible for use of User's account by others. The Terms of Use shall be void where prohibited by applicable laws, and the right to access the Website shall automatically stand revoked in such cases.</p>


<h2>Content in the Services</h2>


<p>For the purposes of these Terms of Use, the term " <strong>Content</strong>" includes, without limitation, information, data, text, logos, photographs, videos, audio clips, animations, written posts, articles, comments, software, scripts, graphics, themes and interactive features generated, provided or otherwise made accessible on or through the Services.</p>
<p>You should be aware that Content presented to you as part of the Services, including but not limited to advertisements in the Services and sponsored Content within the Services may be protected by intellectual property rights which are owned by the sponsors or advertisers who provide that Content to FastGoCash (or by other persons or companies on their behalf). You may not modify, rent, lease, loan, sell, distribute or create derivative works based on this Content (either in whole or in part) unless you have been specifically told that you may do so by FastGoCash or by the owners of that Content, in writing and in a separate agreement.</p>
<p>FastGoCash reserves the right (but shall have no obligation) to pre-screen, review, flag, filter, modify, refuse or remove any or all Content from any Service.</p>
<p>FastGoCash reserves the right to moderate, publish, re-publish, and use all User generated contributions and comments (including but not limited to reviews, profile pictures, comments, likes, favorites, votes) posted on the Website as it deems appropriate (whether in whole or in part) for its product(s), whether owned or affiliated. FastGoCash is not liable to pay royalty to any User for re-publishing any content across any of its platforms.</p>
<p>If you submit any material on the Website, you agree thereby to grant FastGoCash the right to use, moderate, publish any such work worldwide for any of its product(s), whether owned or affiliated.</p>
<p>You understand that by using the Services you may be exposed to Content that you may find offensive, indecent or objectionable and that, in this respect, your use of the Services will be at your own risk.</p>
<p>You agree that you are solely responsible for (and that FastGoCash has no responsibility to you or to any third party for) any Content that you create, transmit or display while using the Services and for the consequences of your actions (including any loss or damage which FastGoCash may suffer) by doing so.</p>


<h2>Proprietary Rights</h2>


<p>You acknowledge and agree that FastGoCash (or FastGoCash's licensors) owns all legal and proprietary right, title and interest in and to the Services, including any intellectual property rights which subsist in the Services (whether those rights happen to be registered or not, and wherever in the world those rights may exist). You further acknowledge that the Services may contain information which is designated confidential by FastGoCash and that you shall not disclose such information without FastGoCash's prior written consent.</p>
<p>Unless you have agreed otherwise in writing with FastGoCash, nothing in the Terms of Use gives you a right to use any of FastGoCash's trade names, trademarks, service marks, logos, domain names, and other distinctive brand features.</p>
<p>Unless you have been expressly authorized to do so in writing by FastGoCash, you agree that in using the Services, you will not use any trade mark, service mark, trade name, logo of any company or organization in a way that is likely or intended to cause confusion about the owner or authorized User of such marks, names or logos.</p>


<h2>Compliance with anti-bribery and anti-corruption laws</h2>


<p>User agrees and unconditionally undertakes to comply with all applicable commercial and public anti-bribery and anti-corruption laws (including but not limited to the provisions of Foreign Corrupt Practices Act, 1977, UK Bribery Act, 2010, Prevention of Corruption Act, 1988, Prevention of Money Laundering Act, 2002, Foreign Contribution (Regulation) Act, 2010, and any amendments thereto) which prohibit the User, its/ his/ her officials, representatives, agents or any other person associated with or acting on behalf of such User from giving, offering, promising to offer, receiving/ accepting or acting in any other manner so as to induce a payment, gift, hospitality or anything of value (either directly or indirectly) whether from within the country or from abroad to government officials, publics servants, regulatory bodies, judicial authorities, persons in positions of authority, elected or contesting electoral candidates, political parties or office bearers thereof or any other third party or person in order to obtain an improper commercial/business advantage of any kind. Government Officials include any government employee, candidate for public office, an employee of government - owned or government – controlled companies, public international organisation and political parties. User also agrees not to give, offer, pay, promise or authorise to give or pay, directly or through any other person, anything of value to anybody for the purpose of inducing or rewarding any favourable action or influencing a decision in favour of the User. The User also unconditionally agrees and undertakes that it is compliant with and shall do/ undertake all acts necessary to continue to be compliant with the provisions of Anti-Money Laundering (AML)/ Combating Financing of Terrorism (CFT) guidelines issued by the Department of Regulation', RBI, as amended from time to time.</p>


<h2>Compliance with mandates of the Office of Foreign Assets Control</h2>


<p>User undertakes that it shall, during the use of the Services, be in compliance with the mandates of OFAC and acknowledges that the User has not directly or indirectly lent, contributed or otherwise made available funds to any of its affiliates, joint venture partners or any other person or entity for the purpose of financing the activities of any person currently subject to the OFAC Specially Designated Nationals List (SDN), Consolidated Sanctions List and the Additional OFAC Sanctions List, as amended from time to time. For the purpose of this paragraph 7, OFAC means the Office of Foreign Assets Control constituted under the laws of the United States of America.</p>


<h2>Exclusion of Warranties</h2>


<p>Nothing in these Terms of Use, including this paragraph 8, shall exclude or limit your warranty or liability for losses which may not be lawfully excluded or limited by applicable law. Some jurisdictions do not allow the exclusion of certain warranties or conditions or the limitation or exclusion of liability for loss or damage caused by negligence, breach of contract or breach of implied terms, or incidental or consequential damages. Accordingly, only the limitations which are lawful in your jurisdiction will apply to you and our liability will be limited to the maximum extent permitted by law. FastGoCash disclaims any implied warranty for Services and any use thereof.</p>
<p>You expressly understand and agree that your use of the Services is at your sole risk and that the Services are provided on an "as is" and "as available" basis.</p>
<p>In particular, FastGoCash, its subsidiaries and affiliates, and its licensors do not represent or warrant to you that:</p>

The information or Contents provided on the Website are accurate, complete and updated;
Your use of the Services will meet your requirements;
Your use of the Services will be uninterrupted, timely, secure or free from error;
Any information obtained by you as a result of your use of the Services will be accurate or reliable; and
That defects in the operation or functionality of any software provided to you as part of the Services will be corrected.

<p>Any material downloaded or otherwise obtained through the use of the Services is done at your own discretion and risk and that you will be solely responsible for any damage to your computer system or other device or loss of data that results from the download of any such material.</p>
<p>No advice or information, whether oral or written, obtained by you from FastGoCash or from the use of Services shall create any warranty not expressly stated in these Terms of Use.</p>
<p>FastGoCash further expressly disclaims all warranties and conditions of any kind, whether express or implied, including, but not limited to the implied warranties and conditions of merchantability, fitness for a particular purpose and non-infringement.</p>


<h2>Representations and Warranties of User/ seller</h2>


<h2>The User/ seller represents and warrants:</h2>

That User/ seller, in case of a natural person, is at least 18 years old with a conscious mind fit and proper to enter into this agreement (the "Terms of Use"), is a resident of India with valid credentials and is an entity who is legally eligible to carry out or operate a business in India;
That the all the information and documents pertaining to his/ her identity and address proof, as submitted for the purpose of know your client (KYC) verification with FastGoCash are true and genuine and are not fabricated or doctored in any way whatsoever;
To hold and keep FastGoCash, its promoters, directors, employees, officials, agents, subsidiaries, affiliates and representatives harmless from any liabilities arising in connection with any incidental or intentional discrepancy that is found to be there in the documents submitted by such User for the purpose of KYC formalities;
That any incidental or upfront liability arising in connection with User's/ seller's KYC formalities for the purpose of availing the services of FastGoCash shall be the absolute responsibility and repercussion of the User and neither FastGoCash nor any of its affiliates or office bearers shall be responsible in any way for any reason including for ascertaining the veracity of the KYC documents submitted by such User with FastGoCash;
That User/ seller shall be solely responsible for understanding and complying with any and all applicable laws relevant to the User and their business, and any liability, whether pecuniary or otherwise, arising from any non-compliance of such applicable laws shall be at the sole cost and risk of such User;
That the User/ seller shall operate and conduct his/ her business as per declaration provided by such User to FastGoCash at the time of onboarding of such User by FastGoCash and shall promptly report any change/ deviation/ addition/ deletion in the scope of business activities of such User to FastGoCash;
That all data, information, inventions, intellectual properties (including patents, trademarks, copyrights, design and trade secrets), know-how, new uses and processes, and any other intellectual property right, asset or form, including, but not limited to, analytical methods, procedures and techniques, research, procedure manuals, financial information, computer technical expertise, software for the purpose of availing of services of FastGoCash and any updates or amendments thereto is and shall be the sole intellectual property of FastGoCash and should in no way be construed to grant any rights and/ or title to the User in such intellectual property of FastGoCash;
That User/ seller shall not store any customer card and such related date in any form or manner whatsoever on their websites/ servers;
That the User/ seller shall accord adequate and timely co-operation in allowing FastGoCash or
it's appointed agencies or regulators to conduct audits to ensure adherence to data security recommendations of such User/ seller under applicable laws; and
That the User/ seller shall be primarily responsible for co-operating with their customers and with FastGoCash for handling customer complaints and the User's/ seller's website shall clearly indicate the terms and conditions of their services and timelines for processing of returns and refunds.



<h2>Indemnity</h2>




<p>The User shall indemnify and hold FastGoCash, its subsidiaries, affiliates, directors, employees, contractors, licensors and agents and any other related or third parties, including but not limited to banks/ wallets/ payment option provider, involved with FastGoCash in any manner whatsoever, harmless from and against all losses arising from claims, demands, actions or other proceedings as a result of:</p>


<p>Fraud, negligence and willful misconduct by the User in the use of the Services;</p>


<p>Violation of applicable laws in the use of the Services and/ or in the conduct of the business of the User, including but not limited to the legal provisions mentioned under paragraphs 6 and 7 hereinabove;</p>


<p>Breach of the User's confidentiality obligations under these Terms of Use;</p>


<p>Disputes raised by a User's customer in relation to a transaction where such dispute is not attributable to the Services; and</p>


<p>Fines, penalties and charges imposed by the Acquiring Bank, Card Payment Networks or any Governmental Authority on account of transactions on the User's website or platform that are in violation of applicable law.</p>




<h2>Limitation of Liability</h2>


<p>Subject to the overall provisions stated above, you expressly understand and agree that FastGoCash, its subsidiaries, affiliates, directors, employees, agents and licensors shall not be liable to you for:</p>

Any direct, indirect, incidental, special, consequential, punitive or exemplary damages which may be incurred by you, however caused and under any theory of liability. This shall include, but not be limited to, any loss of profit (whether incurred directly or indirectly), any loss of goodwill or business reputation, any loss of data suffered, cost of procurement of substitute goods or Services, or other intangible loss;
Any loss or damage which may be incurred by you, including but not limited to loss or damage as a result of any reliance placed by you on the completeness, accuracy or existence of any advertising, or as a result of any relationship or transaction between you and any advertiser or sponsor whose advertisement appears on the Services;
The deletion of, corruption of, or failure to store, any content and other communications data maintained or transmitted by or through your use of the Services;
Your failure to provide FastGoCash with accurate Registration Data; or
Your failure to keep your password or account details secure and confidential.

<p>Total liability of FastGoCash, under any theory of law whatsoever, shall be limited to 1 (one) week's Fees paid to FastGoCash immediately preceding the claim. The limitations on FastGoCash's liability to you shall apply whether or not FastGoCash has/ had been advised of or should have been aware of the possibility of any losses to you.</p>


<h2>Force Majeure</h2>




<p>FastGoCash shall not be in breach of its obligation hereunder if it is delayed in the performance of, or is unable to perform (whether partially or fully) its obligations (provide the Services) as a result of the occurrence of a Force Majeure Event (defined below).</p>


<p>Force Majeure Event means any event, whatever be the origin, not within the reasonable control of FastGoCash, which FastGoCash is unable to prevent, avoid or remove or circumvent by the use of reasonable diligence. Force Majeure event shall include, but shall not be limited to, acts of god, acts of governmental/ regulatory/ judicial authorities, war, hostilities, invasion, armed conflict, act of foreign enemy, embargoes, riot, insurrection, labour stoppages, outages and downtimes systems failures experienced by a facility provider, revolution or usurped power, acts of terrorism, sabotage, nuclear explosion, earthquake, pandemic, epidemic, hacking or man in the middle attack or similar attacks/ intrusions, fires, typhoons, storms and other natural catastrophes.</p>


<p>Any payment obligations of FastGoCash, in case of a Force Majeure event, shall be limited by and be subject to the fulfillment of the payment obligations of the partners banks/ financial institutions, counterparties and any other parties involved in or intrinsically linked to the provision of the Services of FastGoCash.</p>




<h2>Confidentiality</h2>


<p>The User may receive or have access to certain confidential and proprietary information, including without limitation, information belonging and/or relating to FastGoCash and its affiliates, marketing prospects, contractors, officers, directors or shareholders, personal data of customers of the User, financial and operational information, billing records, business model and reports, computer systems and modules, secure websites, reporting systems, marketing strategies, operational plans, proprietary systems and procedures, trade secrets and other similar proprietary information, including technical "know-how", methods of operation, business methodologies, software, software and technology architecture, networks, any other information not generally available to the public, and any items in any form in writing or oral, clearly identified as confidential (" <strong>Confidential Information</strong>").</p>
<p>The User shall keep Confidential Information in confidence. The User shall use commercial, reasonable and necessary safety measures and steps to maintain the confidentiality and secrecy of the Confidential Information from public disclosure, and the User shall at all times maintain appropriate measures to protect the security and integrity of the Confidential Information. The User shall not, without FastGoCash's prior written consent, divulge any of the Confidential Information to any third party other than the User's officers, employees, agents or representatives who have a need to know for the purposes of these Terms of Use. The User shall take all reasonable steps to ensure that all of its directors, managers, officers, employees, agents, independent contractors or other representatives comply with this paragraph 12 whenever they are in possession of Confidential Information as part of this Agreement. The User shall use the Confidential Information solely in furtherance of and in connection with the Services contemplated under these Terms of Use. The User further agrees that the Confidential Information will not be used by him/ her and his/ her representatives, in any way detrimental to the interests of FastGoCash.</p>
<p>Exceptions: The aforesaid confidentiality obligations shall impose no obligation on the User with respect to any portion of Confidential Information which:</p>

Was at the time received or which thereafter becomes, through no act or failure on the part of the User, generally known or available to the public;
Was at the time of receipt, known to the User as evidenced by written documentation then rightfully in the possession of the User or FastGoCash;
Was already acquired by the User from a third party who does not thereby breach an obligation of confidentiality to FastGoCash and who discloses it to the User in good faith;
Was developed by the User without use of the FastGoCash's Confidential Information in such development; or
Has been disclosed pursuant to the requirements of applicable law, any governmental/ regulatory authority, judicial/ quasi-judicial authority provided however, that FastGoCash shall have been given a reasonable opportunity to resist disclosure and/or to obtain a suitable protective order.

<p>The User shall notify FastGoCash immediately upon discovery of any unauthorized use or disclosure of Confidential Information or any other breach of this paragraph 12. The User will cooperate with FastGoCash in every reasonable way to help FastGoCash regain possession of such Confidential Information and prevent its further unauthorized use.</p>
<p>Remedies: Parties acknowledge that irreparable damage may occur on breach of the terms and provisions set out in this paragraph 12. Accordingly, if the User breaches or threatens to breach any of the provisions set out in this paragraph 12, then FastGoCash shall be entitled, without prejudice, to seek all the rights and remedies available to it under applicable law, including a temporary restraining order and an injunction restraining any breach of the provisions set out in this paragraph 12. Such remedies shall not be deemed to be exclusive but shall be in addition to all other remedies available under applicable law or in equity.</p>


<h2>Fraudulent Transactions</h2>


<p>Subject to paragraphs 14.2 and 14.3 below, if FastGoCash is intimated, by the Acquiring Bank or a Card Payment Network, that a customer has reported an unauthorised debit of the customer's payment instrument (" <strong>Fraudulent Transaction</strong>"), then FastGoCash shall be entitled to suspend the settlement of the amount associated with the Fraudulent Transaction during the pendency of inquiries, investigations and resolution thereof by the Acquiring Bank or the Card Payment Network.</p>
<p>Subject to paragraph 14.3 below, if the Fraudulent Transaction results in a Chargeback, then the Chargeback shall be resolved in accordance with the provisions relating to Chargeback as set out under these Terms of Use.</p>
<p>If the amount in respect of a Fraudulent Transaction has already been settled to the User pursuant to these Terms of Use, any dispute arising in relation to the said Fraudulent Transaction, following settlement, shall be resolved in accordance with the RBI's notification DBR.No.Leg.BC.78/09.07.005/2017-18, dated July 6, 2017 read with RBI's notification DBOD. LEG. BC 86/09.07.007/2001-02 dated April 8, 2002 and other notifications, circulars and guidelines issued by the RBI in this regard from time to time.</p>


<h2>Advertising</h2>


<p>Some of the Services are supported by advertising revenue and may display advertisements and promotions. These advertisements may be targeted to the content of information stored on the Services, queries made through the Services or other information.</p>
<p>The manner, mode and extent of advertising by FastGoCash on the Services are subject to change without any specific notice to you.</p>
<p>In consideration for FastGoCash granting you access to and use of the Services, you agree that FastGoCash may place such advertising on the Services.</p>


<h2>Authorisation</h2>


<p>By accepting these Terms of Use, you authorise us to hold, receive, disburse and settle funds on your behalf. Your authorisation permits us to generate an electronic funds transfer between the payment system providers and our nodal account to process each payment transaction that you authorise.</p>
<p>Thereafter, you authorise us to transfer the payments received from your buyers to the bank account designated by you for this purpose at the time of registration (" <strong>Acquiring Bank</strong>"). Your authorisation will remain in full force and effect until your FastGoCash account is closed or terminated.</p>


<h2>Card Association Rules</h2>


<p>" <strong>Card Payment Network Rules</strong>" refer to the written rules, regulations, releases, guidelines, processes, interpretations and other requirements (whether contractual or otherwise) imposed and adopted by the Card Payment Networks. These Card Payment Networks have infrastructure and processes to enable transaction authorisation. The Card Payment Networks require you to comply with all applicable guidelines, rules, and regulations formulated by them.</p>
<p>The Card Payment Networks reserve the right to amend their guidelines, rules and regulations. We may be required to amend modify or change these Terms of Use pursuant to amendments to the Card Payment Network Rules and such amendments, if any, shall be deemed to be binding on the Users with immediate effect.</p>
<p>You agree to fully comply with all programs, guidelines, requirements that may be published and/ or mandated by the Card Payment Networks. Notwithstanding our assistance in understanding the Card Payment Network Rules, you expressly acknowledge and agree that you are assuming the risk of compliance with all provisions of the Card Payment Network Rules, regardless of whether you are aware of or have access to those provisions. MasterCard, Visa and American Express make excerpts of their respective rules available on their internet sites.</p>
<p>In the event that your non-compliance of Card Payment Network Rules, results in any fines, penalties or other amounts being levied on or demanded of us by a Card Payment Network, then without prejudice to our other rights hereunder, you shall forthwith reimburse us in an amount equal to the fines, penalties or other amount so levied or demanded or spent by us in any manner in relation to such fines, penalties and levies. If you fail to comply with your obligations towards the Card Payment Networks, FastGoCash may suspend settlement or suspend/ terminate the Services forthwith.</p>


<h2>Settlements</h2>


<p>In consideration of the Services rendered by us, you shall pay FastGoCash a fee ("Fee").  FastGoCash reserves the right to revise the Fee periodically and will intimate you of any such change within reasonable time. On receipt of the payments in the nodal account, we will endeavour to instruct the nodal bank to transmit the payments payable to the seller, after deducting our Fee, from the nodal account to the seller's designated bank account, within 3 (three) bank working days (or such other period as may be prescribed by the Reserve Bank of India from time to time) from completion of transaction. Subject to any other provisions of these Terms of Use and completion of transaction, the seller acknowledges that we will settle the payments only upon actual receipt of payments in the nodal account and upon reconciliation of the payments by the Acquiring Banks, our payment gateway and the nodal bank. The seller will bear and be responsible and liable for the payment of all relevant taxes in relation to the payments made under these Terms of Use.</p>
<p>Sellers receive the amount (minus FastGoCash's Fee) in their bank account within T+3 bank working days where T is defined as the date of intimation of the completion of the transaction. FastGoCash shall be entitled to charge on the Fee, taxes applicable from time to time (" <strong>Applicable Taxes</strong>"). It is agreed that any statutory variations in Applicable Taxes during the subsistence of this Agreement shall be borne by the User.</p>
<p>Monthly invoices shall be raised by us in respect of the Fees charged for transactions processed during such month. Any reasonable dispute in respect of an amount (or a portion thereof) mentioned in an invoice must be communicated by the User via notice (" <strong>Invoice Dispute Notice</strong>") within a reasonable period of time but no later than thirty (30) calendar days from the date of the invoice. FastGoCash shall use good faith efforts to reconcile any reasonably disputed amounts within reasonable time from the receipt of the Invoice Dispute Notice.</p>
<p>In respect of invoices received by the Client, it is agreed that if the User pays over applicable taxes under the Indian Income Tax laws and furnishes to us the TRACES certificate in respect of such taxes paid, then we shall reimburse to the User, on a quarterly basis, the amount in respect of such taxes paid.</p>
<p>Once a payment is authenticated by payment service providers, money shall be moved to our nodal account and the first settlement shall be initiated only after all required documents (in hard copies) are received by FastGoCash.</p>
<p>All risks associated with the delivery of the goods or service will solely be that of the seller and not of FastGoCash. Also, all disputes regarding quality, merchantability, non-delivery, delay in delivery or otherwise will be directly between the seller and the buyer without making FastGoCash and/ or the payment service providers, a party to such disputes.</p>
<p>Further, FastGoCash also reserves the right to close, suspend, limit or put on hold the User's access to the account with FastGoCash and/ or the funds available therein, including settlements pertaining to the User under <em>inter alia</em> the following scenarios:</p>

If such User's KYC credentials are found to be ingenuine or fake;
For violation of any of the provisions of these "Terms of Use";
For violation of any of the provisions of any other agreement that the User has entered into or might enter into with FastGoCash; and
For violation of any of the applicable laws by such User.

<p>Such right to close, suspend, limit or put on hold the User's access to the account with FastGoCash shall continue till such time that such User submits genuine KYC documents or credentials to the satisfaction of the relevant authorities as per the extant rules, regulations or guidelines with regard to KYC, as well as to the satisfaction of FastGoCash without prejudice to any other legal remedy that FastGoCash is entitled to prefer as per applicable law.</p>


<h2>Prohibited Services</h2>


<p>You agree that you will not accept payments in connection with businesses, business activities or business practices, including but limited to the following:</p>


<p>Adult goods and services which includes pornography and other sexually suggestive materials (including literature, imagery and other media); escort or prostitution services; Website access and/or website memberships of pornography or illegal sites;</p>


<p>Alcohol which includes alcohol or alcoholic beverages such as beer, liquor, wine, or champagne;</p>


<p>Body parts which includes organs or other body parts;</p>


<p>Bulk marketing tools which includes email lists, software, or other products enabling unsolicited email messages (spam);</p>


<p>Cable descramblers and black boxes which includes devices intended to obtain cable and satellite signals for free;</p>


<p>Child pornography which includes pornographic materials involving minors;</p>


<p>Copyright unlocking devices which includes mod chips or other devices designed to circumvent copyright protection;</p>


<p>Copyrighted media which includes unauthorized copies of books, music, movies, and other licensed or protected materials;</p>


<p>Copyrighted software which includes unauthorized copies of software, video games and other licensed or protected materials, including OEM or bundled software;</p>


<p>Counterfeit and unauthorized goods which includes replicas or imitations of designer goods; items without a celebrity endorsement that would normally require such an association; fake autographs, counterfeit stamps, and other potentially unauthorized goods;</p>


<p>Drugs and drug paraphernalia which includes illegal drugs and drug accessories, including herbal drugs like marijuana, salvia and magic mushrooms etc.;</p>


<p>Drug test circumvention aids which includes drug cleansing shakes, urine test additives, and related items;</p>


<p>Endangered species which includes plants, animals or other organisms (including product derivatives) in danger of extinction;</p>


<p>Gaming/gambling which includes lottery tickets, sports bets, memberships/ enrolment in online gambling sites, and related content;</p>


<p>Government IDs or documents which includes fake IDs, passports, diplomas, and noble titles;</p>


<p>Hacking and cracking materials which includes manuals, how-to guides, information, or equipment enabling illegal access to software, servers, website, or other protected property;</p>


<p>Illegal goods which include materials, products, or information promoting illegal goods or enabling illegal acts;</p>


<p>Miracle cures which includes unsubstantiated cures, remedies or other items marketed as quick health fixes;</p>


<p>Offensive goods which includes literature, products or other materials that <em>inter alia</em>:</p>

Defame or slander any person or groups of people based on race, ethnicity, national origin, religion, sex, or other factors;
Encourage or incite violent acts; or
Promote intolerance or hatred.



<p>Offensive goods, crime which includes crime scene photos or items, such as personal belongings, associated with criminals;</p>


<p>Pyrotechnic devices, combustibles, corrosives and hazardous materials which includes explosives and related goods; toxic, flammable, and radioactive materials and substances;</p>


<p>Regulated goods which includes air bags; batteries containing mercury; Freon or similar substances/refrigerants, chemical/industrial solvents, government uniforms, car titles, license plates, police badges and law enforcement equipment, lock-picking devices, pesticides, postage meters, recalled items, slot machines, surveillance equipment, goods regulated by government or other agency specifications;</p>


<p>Securities which includes government and/ or public sector unit bonds, stocks, debentures or related financial products;</p>


<p>Tobacco and cigarettes which include cigarettes, cigars, chewing tobacco, and related products;</p>


<p>Traffic devices which include radar detectors/ jammers, license plate covers, traffic signal changers, and related products;</p>


<p>Weapons which include firearms, ammunition, knives, brass knuckles, gun parts, gun powder or explosive mixtures and other armaments;</p>


<p>Wholesale currency which includes discounted currencies or currency exchanges;</p>


<p>Live animals or hides/ skins/ teeth, nails and other parts etc. of animals;</p>


<p>Multi-level marketing collection fees;</p>


<p>Matrix sites or sites using a matrix scheme approach;</p>


<p>Work-at-home approach and/ or work-at-home information;</p>


<p>Drop-shipped merchandise;</p>


<p>Any product or service which is not in compliance with all applicable laws and regulations whether federal, state, local or international, including the laws of India;</p>


<p>The User providing services that have the potential of casting the payment gateway facilitators in a poor light and/ or that may be prone to buy and deny attitude of the cardholders when billed (e.g. adult material/ mature content/ escort services/ friend finders) and thus leading to chargeback and fraud losses;</p>


<p>Businesses or website that operate within the scope of laws which are not absolutely clear or are ambiguous in nature (e.g. web-based telephony, website supplying medicines or controlled substances, website that promise online match-making);</p>


<p>Businesses out rightly banned by law (e.g. betting &amp; gambling/ publications or content that is likely to be interpreted by the authorities as leading to moral turpitude or decadence or incite caste/ communal tensions, lotteries/ sweepstakes &amp; games of chance;</p>


<p>The User who deal in intangible goods/ services (e.g. software download/ health/ beauty Products), and businesses involved in pyramid marketing schemes or get-rich-quick schemes;</p>


<p>Any other product or service, which in the sole opinion of either the Acquiring Bank, is detrimental to the image and interests of either of them / both of them, as communicated by either of them/ both of them to the User from time to time. This shall be without prejudice to any other terms &amp; conditions mentioned in these Terms of Use;</p>


<p>Mailing lists;</p>


<p>Virtual currency, cryptocurrency, prohibited investments for commercial gain or credits that can be monetized, re-sold or converted to physical or digital goods or services or otherwise exit the virtual world;</p>


<p>Money laundering services;</p>


<p>Database providers (for tele-callers);</p>


<p>Bidding/ auction houses;</p>


<p>Activities prohibited by the Telecom Regulatory Authority of India; and</p>


<p>Any other activities prohibited by applicable law.</p>


<p>The above list is subject to additions/ amendments (basis changes/ amendments to applicable laws) by FastGoCash without prior intimation to you.</p>


<h2>Transaction Disputes</h2>


<p>Transactions may be disputed anytime within up to 120 (one hundred twenty) days, from the date of transaction by a buyer, as per the Card Payment Network Rules. Disputes resolved in favour of a buyer may result in reversal of payment to such buyer (" <strong>Chargeback</strong>"). In the event of rejection/ suspension of payments to the seller, chargebacks, refunds and/or any other dispute relating to the transactions contemplated under these Terms of Use (" <strong>Disputed Transaction</strong>"), on any grounds whatsoever, we will forthwith notify the seller of the same.</p>
<p>On such notification the seller will conduct an internal review of such matter and will, within 5 (five) working days from receipt of notification, respond to us in writing either:</p>

Requesting us to refund (" <strong>Refund Request</strong>") the payment received by the seller in respect of such Disputed Transaction (" <strong>Refund Monies</strong>"); or
Providing us with a statement explaining how the Disputed Transaction is not warranted, together with all documentary evidence in support of contesting such Disputed Transaction.

<p>All refunds shall be made to the original method of payment. In the event that the seller provides a Refund Request to us or fails to contest such Disputed Transaction within the aforesaid 5 (five) working days or contests Disputed Transaction without providing supporting documentation to us, payment service providers, Card Payment Network and/ or issuing institution's satisfaction, we will be entitled to recover the Refund Monies from credits subsequently made to the nodal account with respect to payments made by the seller's buyers.</p>
<p>In the event that we are unable to recover the Refund Monies as aforesaid, due to the amounts credited to the nodal account being lower than the Refund Monies, FastGoCash shall be entitled to recover such Refund Monies (or any part thereof) from the User by (i) raising a debit note in respect of such monies; and/ or (ii) setting-off the remaining Refund Monies against the future payables to the seller and refund the same to the respective buyers. The seller will be liable to make payment of the Refund Monies or part thereof which has not been recovered by us forthwith. It is hereby agreed and acknowledged by the parties that the Fees charged by us in respect of the Disputed Transaction will not be refunded or repaid by us to the seller, buyer or any other person. Further, the Chargeback will be provided within 1 (one) week of the transaction and maximum amount of the Chargeback payable by FastGoCash to the buyer will be the value of the transaction only.</p>


<h2>Technical Issues &amp; Delivery Policy</h2>


<p>In case of any technical issues, please raise a support ticket from your service dashboard. You can also contact us on 1800-2700-323 in case of urgent matters. We endeavour to deliver Service to you within 15 (fifteen) working days of bank approval, failing which you can terminate a transaction related to Service at any time and get a full refund.</p>


<h2>Governing Law, Settlement of Disputes and Jurisdiction</h2>


<p>These Terms of Use and any dispute or claim arising under it will be governed by and construed in accordance with the laws of India. The Users agree that any legal action or proceedings arising out of these Terms of Use or in connection with these Terms of Use may be brought exclusively in the competent courts/ tribunals having jurisdiction in Bengaluru, India and the Users irrevocably submit themselves to the jurisdiction of such courts/ tribunals.</p>


<h2>Privacy</h2>


<p>Your privacy is extremely important to us. Upon acceptance of these Terms of Use you confirm that you have read, understood and unequivocally accepted our Privacy Policy <strong>.</strong></p>






</div>


</asp:Content>

