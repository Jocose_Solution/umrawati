﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DMT-Manager/MasterPagefor_money-transfer.master" AutoEventWireup="true" CodeFile="dmt-direct.aspx.cs" Inherits="DMT_Manager_dmt_direct" %>

<%@ Register Src="~/DMT-Manager/User_Control/dmtdirect/dmt-direct.ascx" TagPrefix="uc1" TagName="dmtdirect" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="custom/css/validationcss.css" rel="stylesheet" />

    <section class="hero-wrap section shadow-md py-4">
        <div class="hero-mask opacity-7 bg-dark"></div>
        <div class="hero-bg" style="background-image: url('images/bg/image-6.jpg');"></div>
        <div class="hero-content py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-xl-10 my-auto" style="margin-left: auto; margin-right: auto;">
                        <div class="bg-white rounded shadow-md p-4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3 class="text-5 text-center">Enter Sender’s Mobile Number : <span style="color:#ff414d;">DMT Direct</span></h3>
                                </div>
                            </div>
                            <hr class="mb-4" />
                            <div class="row">
                                <div class="col-sm-9 col-lg-offcet-3" style="margin-left: auto; margin: auto;">
                                    <div class="row">
                                        <div class="col-lg-8 form-validation">
                                            <input type="text" id="txtDMTDSenderMobileNo" value="" class="form-control sendmobileno" placeholder="Sender Mobile No." maxlength="10" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
                                            <p style="color: #ccc;">(10 digits) Please don not use prefix zero (0)</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <span id="btnDMTDRemitterMobileSearch" style="cursor: pointer;" class="btn btn-primary btn-block" onclick="DMTDRemitterMobileSearch();">Search</span>
                                            <p class="text-danger" id="DMTDTokenExpired"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hero-content py-5">
            <div class="container">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br />
                                <h3 class="text-5 text-center">Step 1</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Register a Sender</h3>
                                <p>Fill in basic information and register a Sender for Money Transfer. Upgrade Sender to KYC by uploading a Photo Id and an Address Proof.</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br />
                                <h3 class="text-5 text-center">Step 2</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Add a Beneficiary</h3>
                                <p>Add multiple Beneficiaries / Receivers against each Sender.</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br />
                                <h3 class="text-5 text-center">Step 3</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Transfer Money</h3>
                                <p>Instantly begin transferring money.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <uc1:dmtdirect runat="server" ID="dmtdirect" />

    <script src="custom/js/dmt-direct-pay.js"></script>
    <script src="custom/js/common.js"></script>
</asp:Content>
