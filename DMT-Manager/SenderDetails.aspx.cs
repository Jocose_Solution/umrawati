﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using InstantPayServiceLib;
using Newtonsoft.Json.Linq;

public partial class DMT_Manager_dmt_SenderDetails : System.Web.UI.Page
{
    private static string UserId { get; set; }
    private static string Mobile { get; set; }
    private static string RemitterId { get; set; }

    public string RemitterDetail { get; set; }
    public string RemitterBenDetail { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            UserId = Session["UID"].ToString();

            Mobile = Request.QueryString["mobile"] != null ? Request.QueryString["mobile"].ToString() : string.Empty;
            RemitterId = Request.QueryString["sender"] != null ? Request.QueryString["sender"].ToString() : string.Empty;

            if (string.IsNullOrEmpty(UserId) && string.IsNullOrEmpty(Mobile) && string.IsNullOrEmpty(RemitterId))
            {
                Response.Redirect("");
            }
            else if (string.IsNullOrEmpty(Mobile) && string.IsNullOrEmpty(RemitterId))
            {
                Response.Redirect("/dmt-manager/senderindex.aspx");
            }
            else
            {
                if (Session["RemtIdSearch"] != null)
                {
                    BindRemitterBeniDetails(UserId, Mobile, RemitterId);
                }
                else
                {
                    Response.Redirect("/dmt-manager/senderindex.aspx");
                }
            }
        }
        else
        {
            Response.Redirect("/");
        }
    }

    #region [Page Load Active Method]
    private void BindRemitterBeniDetails(string agentId, string regmobile, string remitterid)
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(agentId))
        {
            if (!string.IsNullOrEmpty(regmobile) && !string.IsNullOrEmpty(remitterid))
            {
                DataTable dtRemitter = InstantPay_ApiService.GetT_InstantPayRemitterRegResponse(agentId, remitterid, regmobile);
                if (dtRemitter != null && dtRemitter.Rows.Count > 0)
                {
                    string sendername = dtRemitter.Rows[0]["name"].ToString();
                    string mobile = dtRemitter.Rows[0]["mobile"].ToString();
                    string address = dtRemitter.Rows[0]["address"].ToString() + ", " + dtRemitter.Rows[0]["state"].ToString() + ", " + dtRemitter.Rows[0]["pincode_res"].ToString();
                    string creditLimit = dtRemitter.Rows[0]["CreditLimit"].ToString();
                    string remainingLimit = dtRemitter.Rows[0]["remaininglimit"].ToString();
                    string consumedAmount = dtRemitter.Rows[0]["consumedlimit"].ToString();
                    string localAddress = !string.IsNullOrEmpty(dtRemitter.Rows[0]["CurrentAddress"].ToString()) ? dtRemitter.Rows[0]["CurrentAddress"].ToString() : "- - -";

                    RemitterDetail = SenderHTMLDetails(sendername, mobile, address, localAddress, creditLimit, remainingLimit, consumedAmount);
                }

                RemitterBenDetail = SenderBenHTMLDetails(remitterid, agentId);
            }
        }
    }
    #endregion

    #region [Json Section]    

    [WebMethod]
    public static List<string> BeneficiaryRegistration(string accountno, string bankname, string ifsccode, string name)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    string response = InstantPay_ApiService.BeneficiaryRegistration(RemitterId, name, Mobile, accountno, bankname, ifsccode, UserId);
                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyResult = JObject.Parse(response);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            //if (ReBindRemitterApiDetail())
                            //{
                            result.Add("success");
                            result.Add(SenderBenHTMLDetails(RemitterId, UserId));
                            //}
                        }
                        else
                        {
                            result.Add("failed");
                            result.Add(statusMessage);
                        }
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> GetTransactionHistory()
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    //DataTable dtTrans = InstantPay_ApiService.GetTransactionHistory(Mobile, RemitterId);
                    string fromdate = string.Empty;
                    DateTime currDate = DateTime.Now;
                    fromdate = currDate.ToString("dd/MM/yyyy");

                    DataTable dtTrans = InstantPay_ApiService.GetFilterTransactionHistory(RemitterId, fromdate, "", "", "");

                    result.Add("success");
                    result.Add(BindTransDetails(dtTrans, "today"));
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    private static string BindTransDetails(DataTable dtTrans, string datepos)
    {
        StringBuilder sbTrans = new StringBuilder();

        if (dtTrans != null && dtTrans.Rows.Count > 0)
        {
            for (int i = 0; i < dtTrans.Rows.Count; i++)
            {
                string refund_html = "- - -";
                string status = dtTrans.Rows[i]["Status"].ToString();

                bool isRefund = dtTrans.Rows[i]["Refund"].ToString().ToLower() == "true" ? true : false;

                if (status.ToLower().Trim() == "transaction under process")
                {
                    status = "<td class='text-warning'>UNDER PROCESS</td>";
                }
                else if (status.ToLower().Trim() == "transaction successful")
                {
                    status = "<td class='text-success'>SUCCESS</td>";
                }
                else if (status.ToLower().Trim() == "duplicate transaction")
                {
                    status = "<td class='text-danger'>DUPLICATE</td>";
                    if (isRefund)
                    {
                        refund_html = "<span style='color: blue;font-weight: bold;'>REFUND</span>";
                    }
                    else
                    {
                        refund_html = "<span class='btn btn-primary' data-reportid='" + dtTrans.Rows[i]["RefundId"].ToString() + "' data-amount='" + dtTrans.Rows[i]["Amount"].ToString() + "' data-transid='" + dtTrans.Rows[i]["FundTransId"].ToString() + "' data-trackid='" + dtTrans.Rows[i]["TrackId"].ToString() + "' id='btnRefund_" + dtTrans.Rows[i]["FundTransId"].ToString() + "' onclick='RefundFaildAmount(" + dtTrans.Rows[i]["FundTransId"].ToString() + ");' style='padding: 0rem 0.3rem;font-size: 12px;'>Refund</span>";
                    }
                }
                else if (status.ToLower().Trim() == "transaction failed")
                {
                    status = "<td class='text-danger'>FAILED</td>";
                    if (isRefund)
                    {
                        refund_html = "<span style='color: blue;font-weight: bold;'>REFUND</span>";
                    }
                    else
                    {
                        refund_html = "<span class='btn btn-primary' data-reportid='" + dtTrans.Rows[i]["RefundId"].ToString() + "' data-amount='" + dtTrans.Rows[i]["Amount"].ToString() + "' data-transid='" + dtTrans.Rows[i]["FundTransId"].ToString() + "' data-trackid='" + dtTrans.Rows[i]["TrackId"].ToString() + "' id='btnRefund_" + dtTrans.Rows[i]["FundTransId"].ToString() + "' onclick='RefundFaildAmount(" + dtTrans.Rows[i]["FundTransId"].ToString() + ");' style='padding: 0rem 0.3rem;font-size: 12px;'>Refund</span>";
                    }
                }
                else
                {
                    status = "<td>--</td>";
                    if (isRefund)
                    {
                        refund_html = "<span style='color: blue;font-weight: bold;'>REFUND</span>";
                    }
                    else
                    {
                        refund_html = "<span class='btn btn-primary' data-reportid='" + dtTrans.Rows[i]["RefundId"].ToString() + "'  data-amount='" + dtTrans.Rows[i]["Amount"].ToString() + "' data-transid='" + dtTrans.Rows[i]["FundTransId"].ToString() + "' data-trackid='" + dtTrans.Rows[i]["TrackId"].ToString() + "' id='btnRefund_" + dtTrans.Rows[i]["FundTransId"].ToString() + "' onclick='RefundFaildAmount(" + dtTrans.Rows[i]["FundTransId"].ToString() + ");' style='padding: 0rem 0.3rem;font-size: 12px;'>Refund</span>";
                    }
                }

                sbTrans.Append("<tr>");
                sbTrans.Append("<td>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[i]["UpdatedDate"].ToString()) + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["ipay_id"].ToString() + "</td>");
                //sbTrans.Append("<td>" + dtTrans.Rows[i]["orderid"].ToString() + "</td>");
                //sbTrans.Append("<td>" + dtTrans.Rows[i]["ref_no"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["TrackId"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["TxnMode"].ToString() + "</td>");
                sbTrans.Append("<td>₹ " + dtTrans.Rows[i]["Amount"].ToString() + "</td>");
                sbTrans.Append("<td>₹ " + dtTrans.Rows[i]["charged_amt"].ToString() + "</td>");
                if (dtTrans.Rows[i]["Type"].ToString() == "fund")
                {
                    sbTrans.Append("<td><span class='text-primary' title='View' style='cursor: pointer;' onclick='ShowBenDetails(" + dtTrans.Rows[i]["BenificieryId"].ToString() + ")'>" + dtTrans.Rows[i]["BenName"].ToString() + "</span></td>");
                }
                else
                {
                    sbTrans.Append("<td><span class='text-primary'>" + dtTrans.Rows[i]["BenName"].ToString() + "</span></td>");
                }
                sbTrans.Append(status);
                sbTrans.Append("<td>" + refund_html + "</td>");
                string url = "/dmt-manager/printtrans.aspx?mobile=" + Mobile + "&sender=" + RemitterId + "&beneficaryid=" + dtTrans.Rows[i]["BenificieryId"].ToString() + "&trackid=" + dtTrans.Rows[i]["TrackId"].ToString() + "";

                if (dtTrans.Rows[i]["Type"].ToString() == "fund")
                {
                    sbTrans.Append("<td><a href='" + url + "' target='_blank' class='text-primary'>Print</a></td>");
                }
                else
                {
                    sbTrans.Append("<td>- - -</td>");
                }
                sbTrans.Append("</tr>");
            }
        }
        else
        {
            if (datepos == "today")
            {
                sbTrans.Append("<tr>");
                sbTrans.Append("<td colspan='12' class='text-danger text-center'>Today's record not found !</td>");
                sbTrans.Append("</tr>");
            }
            else
            {
                sbTrans.Append("<tr>");
                sbTrans.Append("<td colspan='12' class='text-danger text-center'>Record not found !</td>");
                sbTrans.Append("</tr>");
            }
        }

        return sbTrans.ToString();
    }

    public static string ConvertStringDateToStringDateFormate(string date)
    {
        DateTime dtDate = new DateTime();

        if (!string.IsNullOrEmpty(date))
        {
            dtDate = DateTime.Parse(date);
            return dtDate.ToString("dd MMM yyyy hh:mm tt");
        }

        return string.Empty;
    }

    [WebMethod]
    public static string GetBeneficiaryDetailById(string benid)
    {
        StringBuilder result = new StringBuilder();

        if (!string.IsNullOrEmpty(UserId))
        {
            if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
            {
                DataTable dtBenDetail = InstantPay_ApiService.GetBenDetailsByRemitterId(RemitterId, UserId, benid);
                if (dtBenDetail != null && dtBenDetail.Rows.Count > 0)
                {
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Beneficary ID</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["BeneficiaryId"].ToString() + "</div></div>");
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Name</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["Name"].ToString() + "</div></div>");
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Account</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["Account"].ToString() + "</div></div>");
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>IFSC Code</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["IfscCode"].ToString() + "</div></div>");
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Bank</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["Bank"].ToString() + "</div></div>");
                }
            }
        }

        return result.ToString();
    }

    [WebMethod]
    public static List<string> GetFilterTransactionHistory(string fromdate, string todate, string trackid, string filstatus)
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(UserId))
        {
            if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
            {
                DataTable dtTrans = InstantPay_ApiService.GetFilterTransactionHistory(RemitterId, fromdate, todate, trackid, filstatus);

                result.Add("success");
                result.Add(BindTransDetails(dtTrans, "filter"));
            }
        }

        return result;
    }

    [WebMethod]
    public static List<string> DeleteBeneficialRecord(string benid)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    string response = InstantPay_ApiService.BeneficiaryRemove(benid, RemitterId, UserId);

                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyResult = JObject.Parse(response);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "otp sent successfully")
                        {
                            result.Add("success");
                        }
                        else if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            result.Add("success");
                        }
                        else if (statusCode.ToLower() == "err" && statusMessage.ToLower() == "invalid beneficiary id")
                        {
                            result.Add("success");
                        }
                        else
                        {
                            result.Add("failed");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> DeleteBeneficiaryVarification(string benid, string otp)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    string response = InstantPay_ApiService.BeneficiaryRemoveValidate(benid, RemitterId, otp, UserId);

                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyResult = JObject.Parse(response);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            result.Add("success");
                            result.Add(SenderBenHTMLDetails(RemitterId, UserId));
                        }
                        else
                        {
                            result.Add("failed");
                            result.Add(statusMessage);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }
    #endregion

    private static bool ReBindRemitterApiDetail()
    {
        bool isSucces = false;

        if (!string.IsNullOrEmpty(UserId))
        {
            if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
            {
                string result = InstantPay_ApiService.GetReSendRemitterDetail(Mobile, UserId);

                if (!string.IsNullOrEmpty(result))
                {
                    dynamic dyResult = JObject.Parse(result);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic remitter = dyData.remitter;
                        string remitterId = remitter.id;

                        dynamic beneficiary = dyData.beneficiary;

                        if (beneficiary != null)
                        {
                            foreach (var item in beneficiary)
                            {
                                string bank = item.bank;
                                string imps = item.imps;
                                string lastSuccessDate = item.last_success_date;
                                string lastSuccessImps = item.last_success_imps;
                                string lastSuccessName = item.last_success_name;

                                bool isUpdated = InstantPay_DataBase.Update_T_InstantPayBeneficary(remitterId, bank, imps, lastSuccessDate, lastSuccessImps, lastSuccessName);
                                if (!isUpdated)
                                {
                                    isSucces = false;
                                    break;
                                }
                                else
                                {
                                    isSucces = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        return isSucces;
    }

    #region [Html Section Bind]
    public static string SenderHTMLDetails(string sendername, string mobile, string address, string localadd, string creditLimit, string remainingLimit, string consumedAmount)
    {
        StringBuilder sbSender = new StringBuilder();

        sbSender.Append("<div class='row boxSender'>");
        sbSender.Append("<div class='col-sm-6'><p class='p_botm'>Sender Details</p></div>");
        sbSender.Append("<div class='col-sm-6' style='padding: 2px;'><div class='row'><div class='col-sm-6'><p class='p_botm'>" + sendername.ToUpper() + " (" + mobile + ")</p></div><div class='col-sm-6 text-right'><p class='p_botm btn btn-sm' style='border: 1px solid #fff;color: #fff;cursor: pointer;padding: 6px;' id='UpdateLocalAddress'><i class='fa fa-edit'></i> Edit local Address</p></div></div></div>");
        //sbSender.Append("<div class='col-sm-6'><p class='p_botm'>" + sendername.ToUpper() + " (" + mobile + ")</p></div>");
        sbSender.Append("</div>");

        sbSender.Append("<div class='row boxSenderdetail'>");
        sbSender.Append("<div class='col-sm-6'><p class='pmargin'>Name</p><p class='pmargin'>Address</p><p class='pmargin'>Current / Local Address</p><p class='pmargin'>Ledger Detail</p></div>");
        sbSender.Append("<div class='col-sm-6'><p class='pmargin'>" + sendername.ToUpper() + " (" + mobile + ")</p><p class='pmargin'>" + address + "</p><p class='pmargin'>" + localadd + "</p> <p class='pmargin'>Credit Limit: " + creditLimit + "/- ConsumedAmount: " + consumedAmount + "/- RemainingLimit: " + remainingLimit + "/-</p></div>");
        sbSender.Append("</div>");

        return sbSender.ToString();
    }

    public static string SenderBenHTMLDetails(string remitterId, string agentid)
    {
        StringBuilder sbSender = new StringBuilder();

        DataTable dtBenDetail = InstantPay_ApiService.GetBenDetailsByRemitterId(remitterId, agentid);

        if (dtBenDetail != null && dtBenDetail.Rows.Count > 0)
        {
            for (int i = 0; i < dtBenDetail.Rows.Count; i++)
            {
                string benid = dtBenDetail.Rows[i]["BeneficiaryId"].ToString();

                sbSender.Append("<tr>");
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["Name"].ToString() + "</td>");
                //sbSender.Append("<td>Other</td>");
                sbSender.Append("<td>" + (!string.IsNullOrEmpty(dtBenDetail.Rows[i]["Bank"].ToString()) ? dtBenDetail.Rows[i]["Bank"].ToString() : (!string.IsNullOrEmpty(dtBenDetail.Rows[i]["ReqBank"].ToString()) ? dtBenDetail.Rows[i]["ReqBank"].ToString() : "- - -")) + "</td>");
                if (dtBenDetail.Rows[i]["Status"].ToString() == "1")
                {
                    sbSender.Append("<td><span class='fa fa-check-circle accountvalid' data-toogle='tooltip' title='Account number is valid'></span></td>");
                }
                else
                {
                    sbSender.Append("<td><span class='fa fa-question-circle text-warning accountvalid' data-toogle='tooltip' title='A/C validation is pending'></span></td>");
                }
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["Account"].ToString() + "</td>");
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["IfscCode"].ToString() + "</td>");
                sbSender.Append("<td><div class='form-validation'> <input type='text' class='form-control textboxamount' id='txtTranfAmount_" + benid + "' name='txtTranfAmount_" + benid + "' /></div></td>");
                sbSender.Append("<td><span class='btn btn-success btn-sm bentransfermoney' style='padding: 5px; font-size: 12px; font-weight: 500; color: #fff!important' id='btnMoneyTransfer_" + benid + "' data-benid='" + benid + "'  onclick='DirectTransferMoney(" + benid + ");'>Transfer</span></td>");
                sbSender.Append("<td><span class='btn btn-danger btn-sm bendeleterecord' style='padding: 5px; font-size: 12px; font-weight: 500; color: #fff!important' id='btnDeleteBeneficiary_" + benid + "' data-benid='" + benid + "' onclick='DeleteBeneficialRecord(" + benid + ");'>Delete</span></td>");
                sbSender.Append("</tr>");
            }
        }
        else
        {
            sbSender.Append("<tr>");
            sbSender.Append("<td colspan='9' class='text-center text-danger'>Record not found !</td>"); ;
            sbSender.Append("</tr>");
        }

        return sbSender.ToString();
    }
    #endregion

    public static string GetLocalIPAddress()
    {
        string ipAddress = string.Empty;

        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                ipAddress = ip.ToString();
            }
        }

        return ipAddress;
    }

    [WebMethod]
    public static string BindAllBank()
    {
        StringBuilder result = new StringBuilder();

        try
        {
            DataTable dtTopBank = InstantPay_ApiService.GetTopBindAllBank();
            DataTable dtBank = InstantPay_ApiService.GetFurtherBindAllBank();

            if (dtTopBank != null && dtTopBank.Rows.Count > 0)
            {
                result.Append("<option value=''>Select Bank</option>");
                for (int i = 0; i < dtTopBank.Rows.Count; i++)
                {
                    string transfertype = !string.IsNullOrEmpty(dtTopBank.Rows[i]["imps_enabled"].ToString()) && dtTopBank.Rows[i]["imps_enabled"].ToString() == "1" ? "IMPS" : "NEFT";
                    result.Append("<option value='" + dtTopBank.Rows[i]["branch_ifsc"].ToString() + "' data-transfertype='" + transfertype + "'>" + dtTopBank.Rows[i]["bank_name"].ToString() + "</option>");
                }

                result.Append("<option value='' disabled>--------------------------</option>");

                if (dtBank != null && dtBank.Rows.Count > 0)
                {
                    for (int j = 0; j < dtBank.Rows.Count; j++)
                    {
                        string transfertype = !string.IsNullOrEmpty(dtBank.Rows[j]["imps_enabled"].ToString()) && dtBank.Rows[j]["imps_enabled"].ToString() == "1" ? "IMPS" : "NEFT";
                        result.Append("<option value='" + dtBank.Rows[j]["branch_ifsc"].ToString() + "' data-transfertype='" + transfertype + "'>" + dtBank.Rows[j]["bank_name"].ToString() + "</option>");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result.ToString();
    }

    [WebMethod]
    public static List<string> UpdateLocalAddress(string updatedaddress)
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(updatedaddress.Trim()))
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    if (InstantPay_ApiService.UpdateLocalAddress(RemitterId, UserId, Mobile, updatedaddress))
                    {
                        result.Add("success");
                        result.Add("Address has been updated successfully.");
                    }
                }
            }
        }
        else
        {
            result.Add("empty");
            result.Add("Please enter current / local address !");
        }

        return result;
    }

    [WebMethod]
    public static List<string> GetFundTransferVeryficationDetail(string benid, string amount)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    StringBuilder sbTrans = new StringBuilder();

                    DataTable dtBankDetail = new DataTable();
                    DataTable dtFundTransDetail = InstantPay_ApiService.GetFundTransferVeryficationDetail(RemitterId, benid, ref dtBankDetail);
                    if (dtFundTransDetail != null && dtFundTransDetail.Rows.Count > 0)
                    {
                        result.Add("success");

                        sbTrans.Append("<div class='row' style='border-bottom: 1px solid #dee2e6; padding: 0.7rem;'>");
                        sbTrans.Append("<div class='col-sm-4'><p>From</p><span>" + dtFundTransDetail.Rows[0]["FirstName"].ToString() + " " + dtFundTransDetail.Rows[0]["LastName"].ToString() + " (" + dtFundTransDetail.Rows[0]["Mobile"].ToString() + ")</span><br/><span>" + dtFundTransDetail.Rows[0]["Address"].ToString() + ", " + dtFundTransDetail.Rows[0]["PinCode"].ToString() + "</span></div>");
                        sbTrans.Append("<div class='col-sm-4' style='text-align: center;'><img src='./images/arrowright.png' style='width: 120px;' /></div>");
                        sbTrans.Append("<div class='col-sm-4'><p>To</p><span>" + dtFundTransDetail.Rows[0]["Name"].ToString() + "</span><br/><span>" + dtFundTransDetail.Rows[0]["Bank"].ToString() + " - (" + dtFundTransDetail.Rows[0]["IfscCode"].ToString() + ")</span><br/><span>A/C - " + dtFundTransDetail.Rows[0]["Account"].ToString() + "</span></div>");
                        sbTrans.Append("</div>");

                        sbTrans.Append("<div class='row' style='border-bottom: 1px solid #dee2e6; padding: 0.7rem;'>");

                        sbTrans.Append("<div class='col-sm-12'><div class='row'>");
                        sbTrans.Append("<div class='col-sm-4'><b>Amount to be Remitted</b></div>");
                        sbTrans.Append("<div class='col-sm-4'></div>");
                        sbTrans.Append("<div class='col-sm-4'><b>Amount to be Collected</b></div>");
                        sbTrans.Append("</div></div>");

                        sbTrans.Append("<div class='col-sm-12' style='padding: 15px;'><div class='row'>");
                        sbTrans.Append("<div class='col-sm-4'>₹ " + amount + "</div><div class='col-sm-4'></div><div class='col-sm-4'>₹ " + amount + "</div>");
                        sbTrans.Append("</div></div>");

                        sbTrans.Append("</div>");

                        sbTrans.Append("<div class='row' style='padding: 1rem;'>");
                        sbTrans.Append("<div class='col-sm-1'><input type='checkbox' class='form-control' id='ChkFundTransCheckboxCheck' checked='checked' value='' style='height: 20px;' /></div>");
                        sbTrans.Append("<div class='col-sm-11'>By clicking the checkbox I accept the above declaraction.</div>");
                        sbTrans.Append("</div>");

                        result.Add(sbTrans.ToString());

                        string remingLimit = InstantPay_ApiService.GetRemitterRemingLimit(RemitterId, UserId, Mobile);
                        if (!string.IsNullOrEmpty(remingLimit))
                        {
                            if (Convert.ToDecimal(remingLimit) >= Convert.ToDecimal(amount))
                            {
                                string imps_enabled = dtBankDetail.Rows[0]["imps_enabled"].ToString();
                                if (imps_enabled == "1")
                                {
                                    result.Add("<div class='col-sm-6'><button type='button' class='btn btn-primary btn-sm' id='btnIMPSTrans' style='width: 100%;' data-benid='" + benid + "' data-transamount='" + amount + "' onclick='IMPSFundTransfer();'>IMPS</button></div><div class='col-sm-6'><button type='button' id='btnNEFTTrans' class='btn btn-primary btn-sm' data-benid='" + benid + "' data-transamount='" + amount + "' style='width: 100%;' onclick='NEFTFundTransfer();'>NEFT</button></div>");
                                }
                                else
                                {
                                    result.Add("<div class='col-sm-6'><button type='button' id='btnNEFTTrans' class='btn btn-primary btn-sm' style='width: 100%;' data-benid='" + benid + "' data-transamount='" + amount + "' onclick='NEFTFundTransfer();'>NEFT</button></div>");
                                }
                            }
                            else
                            {
                                result.Add("<div class='col-sm-12'><span class='btn btn-primary btn-sm' style='width: 100%;'>TRANSACTION NOT POSSIBLE, CREDIT LIMIT IS LESS THAN TRANSFER AMOUNT, PLEASE CHECK !</span></div>");
                            }
                        }
                        else
                        {
                            result.Add("<div class='col-sm-12'><span class='btn btn-primary btn-sm' style='width: 100%;'>THERE IS NO REMITTER CREDIT LIMIT AVAILABLE !</span></div>");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    [WebMethod]
    public static List<string> ProcessToFundTransfer(string benid, string amount, string transmode)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    string agencyName = string.Empty; string agencyCreditLimit = string.Empty;

                    string remingLimit = InstantPay_ApiService.GetRemitterRemingLimit(RemitterId, UserId, Mobile);
                    if (!string.IsNullOrEmpty(remingLimit))
                    {
                        if (Convert.ToDecimal(remingLimit) >= Convert.ToDecimal(amount))
                        {
                            DataTable dtAgency = InstantPay_ApiService.GetAgencyDetailById(UserId);

                            if (dtAgency != null && dtAgency.Rows.Count > 0)
                            {
                                agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();
                                agencyCreditLimit = dtAgency.Rows[0]["Crd_Limit"].ToString();

                                if (Convert.ToDouble(agencyCreditLimit) > 0)
                                {
                                    string transferMode = transmode == "1" ? "IMPS" : "NEFT";

                                    // string trackid = "DMT" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15).ToUpper();

                                    //debit           
                                    string narration = amount + "_Money_Transfer_To_BeneficiaryId_" + benid;
                                    List<string> isLedger = LedgerDebitCreditSection(amount, UserId, agencyName, "Debit", "DMT1 Money Transfer", "Money_Transfer", "DR", narration, "DEBIT NOTE");

                                    StringBuilder sbResult = new StringBuilder();

                                    if (isLedger.Count > 0)
                                    {
                                        string avlBal = isLedger[1];
                                        string trackid = isLedger[0];

                                        sbResult.Append(BindUserSendFundTrasDetail(benid, trackid));

                                        string response = string.Empty;

                                        decimal TotalAmount = Convert.ToDecimal(amount);
                                        decimal LeftAmount = TotalAmount; decimal UsedAmount = 0;
                                        if (TotalAmount > 5000)
                                        {
                                            int quotientAmount = (int)Decimal.Truncate(TotalAmount / 5000);
                                            int remainder = Convert.ToInt32(TotalAmount % 5000);

                                            for (int i = 0; i < quotientAmount; i++)
                                            {
                                                string uniqueAgentId = UserId + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 5).ToUpper();

                                                UsedAmount = UsedAmount + 5000;
                                                response = InstantPay_ApiService.FundTransfer(Mobile, benid, "5000", transferMode, uniqueAgentId, UserId, trackid, RemitterId);
                                                sbResult.Append(BindFundTransactionResponseDetail(response, "5000"));
                                            }

                                            decimal finalAmount = TotalAmount - UsedAmount;
                                            if (finalAmount > 0)
                                            {
                                                string uniqueAgentId = UserId + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 5).ToUpper();

                                                response = InstantPay_ApiService.FundTransfer(Mobile, benid, remainder.ToString(), transferMode, uniqueAgentId, UserId, trackid, RemitterId);
                                                sbResult.Append(BindFundTransactionResponseDetail(response, finalAmount.ToString()));
                                            }
                                        }
                                        else
                                        {
                                            string uniqueAgentId = UserId + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 5).ToUpper();
                                            response = InstantPay_ApiService.FundTransfer(Mobile, benid, amount, transferMode, uniqueAgentId, UserId, trackid, RemitterId);
                                            sbResult.Append(BindFundTransactionResponseDetail(response, amount));
                                        }

                                        sbResult.Append("</div>");
                                        sbResult.Append("</div>");

                                        result.Add("success");
                                        result.Add(sbResult.ToString() + "<div class='col-sm-12 text-right'><a href='/dmt-manager/printtrans.aspx?mobile=" + Mobile + "&amp;sender=" + RemitterId + "&amp;beneficaryid=" + benid + "&amp;trackid=" + trackid + "' target='_blank' style='cursor: pointer;color:#fff;' class='col-sm-4 btn btn-primary '><i class='fa fa-print'></i> Print Receipt</a></div>");

                                        //Send Mail Details
                                        string mailBody = BindTransPrint(benid, trackid);
                                        MailSend(mailBody, trackid);
                                    }
                                    else
                                    {
                                        result.Add("failed");
                                        result.Add("We are unable to transfer money at the moment. Instead of trying again, please contact our call centre to avoid any inconvenience.");
                                    }
                                }
                                else
                                {
                                    result.Add("failed");
                                    result.Add("Insufficient credit limit !");
                                }
                            }
                        }
                        else
                        {
                            result.Add("lowlimit");
                            result.Add("Transaction not possible, credit limit is less than transfer amount, please check !");
                        }
                    }
                    else
                    {
                        result.Add("nolimit");
                        result.Add("There is no remitter credit limit available !");
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    private static string BindUserSendFundTrasDetail(string benid, string trackid)
    {
        StringBuilder sbResult = new StringBuilder();

        if (!string.IsNullOrEmpty(UserId))
        {
            if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
            {
                DataTable dtBankDetail = new DataTable();
                DataTable dtFundTransDetail = InstantPay_ApiService.GetFundTransferVeryficationDetail(RemitterId, benid, ref dtBankDetail);

                if (dtFundTransDetail != null && dtFundTransDetail.Rows.Count > 0)
                {
                    string currDatetime = DateTime.Now.ToString();

                    sbResult.Append(" <div class='row' style='border-bottom: 1px solid #dee2e6; padding: 0.7rem;'>");
                    sbResult.Append("<div class='col-sm-4'><span><b>From</b></span><br/><span>" + dtFundTransDetail.Rows[0]["FirstName"].ToString() + " " + dtFundTransDetail.Rows[0]["LastName"].ToString() + " (" + dtFundTransDetail.Rows[0]["Mobile"].ToString() + ")</span><br/><span>" + dtFundTransDetail.Rows[0]["Address"].ToString() + ", " + dtFundTransDetail.Rows[0]["PinCode"].ToString() + "</span></div>");

                    sbResult.Append("<div class='col-sm-4' style='text-align: center;'>");
                    sbResult.Append("<p class='text-center'>");
                    sbResult.Append("<span class='fa fa-check-circle text-center text-success' style='font-size: 50px;'></span><br />");
                    sbResult.Append("<span class='text-center' style='font-size: 20px;'>Track ID</span><br />");
                    sbResult.Append("<span class='text-center' style='font-size: 15px;color:#ff414d;'>" + trackid + "</span>");
                    //sbResult.Append("<br /><span style='color:#b0b0b0;'>" + ConvertStringDateToStringDateFormate(currDatetime) + "</span>");
                    sbResult.Append("</p>");
                    sbResult.Append("</div>");
                    sbResult.Append("<div class='col-sm-4'><span><b>To</b></span><br/><span>" + dtFundTransDetail.Rows[0]["Name"].ToString() + "</span><br/><span>" + dtFundTransDetail.Rows[0]["Bank"].ToString() + " - (" + dtFundTransDetail.Rows[0]["IfscCode"].ToString() + ")</span><br/><span>A/C - " + dtFundTransDetail.Rows[0]["Account"].ToString() + "</span></div>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='row' style='padding: 1rem;'>");

                    sbResult.Append("<div class='col-sm-12'>");
                    sbResult.Append("<div class='row' style='border-bottom: 1px dotted #ccc;'>");
                    sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'><b>ORDER ID</b></div>");
                    sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'><b>TRANSACTION DATE</b></div>");
                    sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'><b>STATUS</b></div>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='col-sm-12' style='padding: 15px;'>");
                }
            }
        }

        return sbResult.ToString();
    }

    //private static string BindFundTransactionResponseDetail(string response, string amount)
    //{
    //    StringBuilder sbResult = new StringBuilder();

    //    if (!string.IsNullOrEmpty(response))
    //    {
    //        dynamic dyResult = JObject.Parse(response);
    //        string statusCode = dyResult.statuscode;
    //        string statusMessage = dyResult.status;

    //        string currDatetime = dyResult.timestamp;

    //        string isdatathere = string.Empty;
    //        JToken root = JObject.Parse(response);
    //        JToken data = root["data"];
    //        if (data != null)
    //        {
    //            isdatathere = data.ToString();

    //            if (!string.IsNullOrEmpty(isdatathere))
    //            {
    //                dynamic dyData = dyResult.data;

    //                string orderid = dyData.ipay_id;
    //                string reqAmount = dyData.amount;
    //                string debitAmount = dyData.charged_amt;
    //                string balance = dyData.opening_bal;

    //                sbResult.Append("<div class='row' style='margin-bottom: 10px; border-bottom: 1px dotted #ccc;'>");
    //                sbResult.Append("<div class='col-sm-3' style='margin-bottom: 10px;'>" + orderid + "<br/><span style='color:#b0b0b0;'>" + ConvertStringDateToStringDateFormate(currDatetime) + "</span></div>");
    //                sbResult.Append("<div class='col-sm-3' style='margin-bottom: 10px;'>₹ " + reqAmount + "</div>");
    //                sbResult.Append("<div class='col-sm-2' style='margin-bottom: 10px;'>₹ " + debitAmount + "</div>");
    //                sbResult.Append("<div class='col-sm-2' style='margin-bottom: 10px;'>₹ " + balance + "</div>");

    //                if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
    //                {
    //                    sbResult.Append("<div class='col-sm-2 text-success' style='margin-bottom: 10px;font-weight: bold;'>SUCCESS</div>");
    //                }
    //                else if (statusCode.ToLower() == "tup" && statusMessage.ToLower() == "transaction under process")
    //                {
    //                    sbResult.Append("<div class='col-sm-2 text-warning' style='margin-bottom: 10px;font-weight: bold;'>UNDER PROCESS</div>");
    //                }
    //                else if (statusCode.ToLower() == "err" && statusMessage.ToLower() == "transaction failed")
    //                {
    //                    sbResult.Append("<div class='col-sm-2 text-danger' style='margin-bottom: 10px;font-weight: bold;'>FAILED</div>");
    //                }
    //                else if (statusCode.ToLower() == "dtx" && statusMessage.ToLower() == "duplicate transaction")
    //                {
    //                    sbResult.Append("<div class='col-sm-2 text-danger' style='margin-bottom: 10px;font-weight: bold;'>FAILED</div>");
    //                }
    //                sbResult.Append("</div>");
    //            }
    //            else
    //            {
    //                sbResult.Append("<div class='row' style='margin-bottom: 10px; border-bottom: 1px dotted #ccc;'>");
    //                sbResult.Append("<div class='col-sm-3' style='margin-bottom: 10px;'>" + statusMessage + "<br/><span style='color:#b0b0b0;'>" + ConvertStringDateToStringDateFormate(currDatetime) + "</span></div>");
    //                sbResult.Append("<div class='col-sm-3' style='margin-bottom: 10px;'>₹ " + amount + "</div>");
    //                sbResult.Append("<div class='col-sm-2' style='margin-bottom: 10px;'>₹ 0.0</div>");
    //                sbResult.Append("<div class='col-sm-2' style='margin-bottom: 10px;'>- - -</div>");
    //                sbResult.Append("<div class='col-sm-2 text-danger' style='margin-bottom: 10px;'>FAILED</div>");
    //                sbResult.Append("</div>");
    //            }
    //        }
    //    }

    //    return sbResult.ToString();
    //}

    private static string BindFundTransactionResponseDetail(string response, string amount)
    {
        StringBuilder sbResult = new StringBuilder();

        if (!string.IsNullOrEmpty(response))
        {
            dynamic dyResult = JObject.Parse(response);
            string statusCode = dyResult.statuscode;
            string statusMessage = dyResult.status;

            string currDatetime = dyResult.timestamp;

            string isdatathere = string.Empty;
            JToken root = JObject.Parse(response);
            JToken data = root["data"];
            if (data != null)
            {
                isdatathere = data.ToString();

                if (!string.IsNullOrEmpty(isdatathere))
                {
                    dynamic dyData = dyResult.data;

                    string orderid = dyData.ipay_id;
                    string reqAmount = dyData.amount;
                    string debitAmount = dyData.charged_amt;
                    string balance = dyData.opening_bal;

                    sbResult.Append("<div class='row' style='margin-bottom: 10px; border-bottom: 1px dotted #ccc;'>");
                    sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'>" + orderid + "</div>");
                    sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'><span style='color:#b0b0b0;'>" + ConvertStringDateToStringDateFormate(currDatetime) + "</span></div>");

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        sbResult.Append("<div class='col-sm-4 text-success text-center' style='margin-bottom: 10px;font-weight: bold;'>SUCCESS</div>");
                    }
                    else if (statusCode.ToLower() == "tup" && statusMessage.ToLower() == "transaction under process")
                    {
                        sbResult.Append("<div class='col-sm-4 text-warning text-center' style='margin-bottom: 10px;font-weight: bold;'>UNDER PROCESS</div>");
                    }
                    else if (statusCode.ToLower() == "err" && statusMessage.ToLower() == "transaction failed")
                    {
                        sbResult.Append("<div class='col-sm-4 text-danger text-center' style='margin-bottom: 10px;font-weight: bold;'>FAILED</div>");
                    }
                    else if (statusCode.ToLower() == "dtx" && statusMessage.ToLower() == "duplicate transaction")
                    {
                        sbResult.Append("<div class='col-sm-4 text-danger text-center' style='margin-bottom: 10px;font-weight: bold;'>FAILED</div>");
                    }
                    sbResult.Append("</div>");
                }
                else
                {
                    sbResult.Append("<div class='row' style='margin-bottom: 10px; border-bottom: 1px dotted #ccc;'>");
                    sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'>" + statusMessage + "</div>");
                    sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'><span style='color:#b0b0b0;'>" + ConvertStringDateToStringDateFormate(currDatetime) + "</span></div>");
                    sbResult.Append("<div class='col-sm-4 text-danger text-center' style='margin-bottom: 10px;'>FAILED</div>");
                    sbResult.Append("</div>");
                }
            }
        }

        return sbResult.ToString();
    }

    [WebMethod]
    public static List<string> ProcessToReFund(string transid, string amount, string trackid, string reportid)
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(transid) && !string.IsNullOrEmpty(amount) && !string.IsNullOrEmpty(amount))
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    DataTable dtAgency = InstantPay_ApiService.GetAgencyDetailById(UserId);
                    //credit section
                    if (dtAgency != null && dtAgency.Rows.Count > 0)
                    {
                        string agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();

                        string narration = amount + "_Refund_Money_Transfer";
                        List<string> isLedger = LedgerDebitCreditSection(amount, UserId, agencyName, "Credit", "DMT1 Refund Money Transfer", "Money_Transfer", "CR", narration, "CREDIT NOTE");

                        if (isLedger.Count > 0)
                        {
                            if (InstantPay_ApiService.ProcessToReFund(transid, reportid))
                            {
                                result.Add("true");
                            }
                            else
                            {
                                result.Add("false");
                            }

                            DataTable dtTrans = InstantPay_ApiService.GetFilterTransactionHistory(RemitterId, "", "", "", "");
                            result.Add(BindTransDetails(dtTrans, "refund"));
                        }
                    }
                }
            }
        }

        return result;
    }

    [WebMethod]
    public static List<string> GetBeneficiaryNamePayout(string accountno, string transtype, string ifsccode, string bankname)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    string latitude = "28.690430";
                    string longitude = "77.101662";
                    string ipaddress = GetLocalIPAddress();
                    string alert_email = "gandhienterprises5@gmail.com";

                    string remingLimit = InstantPay_ApiService.GetRemitterRemingLimit(RemitterId, UserId, Mobile);
                    if (!string.IsNullOrEmpty(remingLimit))
                    {
                        if (Convert.ToDecimal(remingLimit) >= 3)
                        {
                            DataTable dtAgency = InstantPay_ApiService.GetAgencyDetailById(UserId);

                            if (dtAgency != null && dtAgency.Rows.Count > 0)
                            {
                                string agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();
                                string agencyCreditLimit = dtAgency.Rows[0]["Crd_Limit"].ToString();

                                if (Convert.ToDouble(agencyCreditLimit) > 0)
                                {
                                    string narration = "3_To_Check_Beneficiary_Name";
                                    List<string> isLedger = LedgerDebitCreditSection("3", UserId, agencyName, "Debit", "Check_Beneficiary_Name", "Beneficiary_Name", "DR", narration, "DEBIT NOTE");

                                    StringBuilder sbResult = new StringBuilder();

                                    if (isLedger.Count > 0)
                                    {
                                        string avlBal = isLedger[1];
                                        string trackid = isLedger[0];
                                        string response = InstantPay_ApiService.InitiatePayout(Mobile, trackid, accountno, transtype, ifsccode, latitude, longitude, ipaddress, alert_email, UserId, RemitterId, bankname);

                                        if (!string.IsNullOrEmpty(response))
                                        {
                                            dynamic dyResult = JObject.Parse(response);
                                            string statusCode = dyResult.statuscode;
                                            string statusMessage = dyResult.status;

                                            if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                                            {
                                                dynamic dyData = dyResult.data;
                                                dynamic dypayout = dyData.payout;

                                                string benName = dypayout.name;
                                                result.Add("success");
                                                result.Add(benName);
                                            }
                                            else
                                            {
                                                result.Add("failed");
                                                result.Add(statusMessage);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.ToString());

        }

        return result;
    }

    #region [Email Formate -- Sending Mail]

    private static bool MailSend(string mailBody, string TrackId)
    {
        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    DataTable dtAgency = InstantPay_ApiService.GetAgencyDetailById(UserId);

                    if (dtAgency != null && dtAgency.Rows.Count > 0)
                    {
                        string toEmail = dtAgency.Rows[0]["Email"].ToString();

                        SqlTransactionDom STDOM = new SqlTransactionDom();
                        DataTable MailDt = new DataTable();
                        MailDt = STDOM.GetMailingDetails("MONEY_TRANSFER", UserId).Tables[0];
                        if (MailDt != null && MailDt.Rows.Count > 0)
                        {
                            bool Status = Convert.ToBoolean(MailDt.Rows[0]["Status"].ToString());
                            string subject = "Money Transfer Receipt [Client Ref# " + TrackId + "]";
                            if (Status)
                            {
                                int isSuccess = STDOM.SendMail(toEmail, MailDt.Rows[0]["MAILFROM"].ToString(), MailDt.Rows[0]["BCC"].ToString(), MailDt.Rows[0]["CC"].ToString(), MailDt.Rows[0]["SMTPCLIENT"].ToString(), MailDt.Rows[0]["UserId"].ToString(), MailDt.Rows[0]["Pass"].ToString(), mailBody, subject, "");
                                if (isSuccess > 0)
                                {
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return false;
    }

    private static string BindTransPrint(string beneficaryid, string trackid)
    {
        StringBuilder sbResult = new StringBuilder();

        if (!string.IsNullOrEmpty(UserId))
        {
            if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
            {
                //DataTable dtAgency = InstantPayFun.GetAgencyDetailById(UserId);

                DataTable dtRemitter = InstantPay_ApiService.GetCombieRemitterDetail(UserId, RemitterId, Mobile);

                sbResult.Append("<div style='border: 1px solid #ff414d; width: 800px; border-collapse: initial;'>");
                sbResult.Append("<div class='col-sm-12'>");
                sbResult.Append("<a href='#'><img src='www.fly2njoy.com/Images/gallery/logo(ft).png' style='max-width: 200px;' /></a>");
                sbResult.Append("<span style='float: right!important; text-align: right; padding: 7px;'>");
                sbResult.Append("<span>" + dtRemitter.Rows[0]["FirstName"].ToString() + " " + dtRemitter.Rows[0]["LastName"].ToString() + "</span><br />");
                sbResult.Append("<span>" + dtRemitter.Rows[0]["Address"].ToString() + ", " + dtRemitter.Rows[0]["City"].ToString() + ", " + dtRemitter.Rows[0]["State"].ToString() + ", " + dtRemitter.Rows[0]["PinCode"].ToString() + "</span>");
                sbResult.Append("</span>");
                sbResult.Append("<hr style='text-align: center; border: 1px solid #ff414d; background-color: #ff414d; margin-top: 0px; margin-bottom: 0;' />");
                sbResult.Append("</div>");
                sbResult.Append("<div class='col-sm-12'>");
                sbResult.Append("<h4 style='text-align: center;'>Customer Transaction Receipt</h4>");
                sbResult.Append("<table data-toggle='table' style='width: 800px; border-collapse: initial; border: 1px solid #ff414d; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;'>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Name</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtRemitter.Rows[0]["FirstName"].ToString() + " " + dtRemitter.Rows[0]["LastName"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Mobile Number</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtRemitter.Rows[0]["Mobile"].ToString() + "</td>");
                sbResult.Append("</tr>");

                DataTable dtBen = InstantPay_ApiService.GetBenDetailsByRemitterId(RemitterId, UserId, beneficaryid);

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Name</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["Name"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Account Number</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["Account"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Bank's IFSC</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["IfscCode"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Bank</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["Bank"].ToString() + "</td>");
                sbResult.Append("</tr>");

                DataTable dtTrans = InstantPay_ApiService.GetTransactionHistoryByTrackId(trackid);

                int transCount = dtTrans.Rows.Count;

                if (transCount > 1)
                {
                    decimal totalamount = 0;
                    for (int t = 0; t < dtTrans.Rows.Count; t++)
                    {
                        totalamount = totalamount + Convert.ToDecimal(dtTrans.Rows[t]["Amount"].ToString());
                    }

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Amount</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;font-weight:bold;'>₹ " + totalamount + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr><th colspan='2'>&nbsp;</th><td colspan='2'>&nbsp;</td></tr>");
                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Order ID</th>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Bank Reference ID</th>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Amount</th>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Date &amp; Times</th>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Status</th>");
                    sbResult.Append("</tr>");

                    for (int t = 0; t < dtTrans.Rows.Count; t++)
                    {
                        sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                        sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[t]["orderid"].ToString() + "</td>");
                        sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[t]["ref_no"].ToString() + "</td>");
                        sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;font-weight:bold;'>₹ " + dtTrans.Rows[t]["Amount"].ToString() + "</td>");
                        sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[t]["UpdatedDate"].ToString()) + "</td>");
                        if (!string.IsNullOrEmpty(dtTrans.Rows[t]["Status"].ToString()) && dtTrans.Rows[t]["Status"].ToString().ToLower() == "transaction successful")
                        {
                            sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px; color:#28a745; font-weight:bold;'>" + dtTrans.Rows[t]["Status"].ToString() + "</td>");
                        }
                        else if (!string.IsNullOrEmpty(dtTrans.Rows[t]["Status"].ToString()))
                        {
                            sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px; color:#ff414d; font-weight:bold;'>" + dtTrans.Rows[t]["Status"].ToString() + "</td>");
                        }
                        else
                        {
                            sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px; color:#ff414d; font-weight:bold;'>Transaction Failed</td>");
                        }
                        sbResult.Append("</tr>");
                    }
                }
                else
                {
                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Order ID</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["orderid"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction ID</th>");
                    //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["ipay_id"].ToString() + "</td>");
                    //sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Track ID</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["TrackId"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Amount</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>₹ " + dtTrans.Rows[0]["Amount"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Charged Amount</th>");
                    //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["charged_amt"].ToString() + "</td>");
                    //sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Date & Times</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[0]["UpdatedDate"].ToString()) + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Status</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["Status"].ToString() + "</td>");
                    sbResult.Append("</tr>");
                }

                sbResult.Append("</table>");
                sbResult.Append("<h4 style='padding-left: 5px;'>Note :-</h4>");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>1. Customer transaction charge is minimum of Rs. 10/- and Maximum 1% of the transaction amount.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>2. In case of non-payment to the beneficiary, the customer will receive an SMS with an OTP that he/she needs to present at the agent location where the transaction was initiated.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>3. In case the Agent charges the Customer in excess of the fee/ charges as mentioned in the receipt, he/she should lodge complaint about the same with our Customer Care on Tel. No. 7409455555 or email us at fastgocash@gmail.com.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>4. The receipt is subject to terms and conditions, privacy policy and terms of use detailed in the website www.fastgocash.com and shall be binding on the Customer for each transaction.</p><br /><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>5. This is a system generated receipt hence does not require any signature.</p><br /><br />");

                sbResult.Append("</div>");
                sbResult.Append("</div>");
            }
        }

        return sbResult.ToString();
    }
    #endregion

    #region [Ledger Deduct and Refund Section]
    private static List<string> LedgerDebitCreditSection(string amount, string agentid, string agencyName, string actiontype, string Remark, string shortRemark, string Uploadtype, string narration, string ledtype)
    {
        List<string> result = new List<string>();

        try
        {
            double Debit = actiontype.ToLower() == "debit" ? Convert.ToDouble(amount) : 0;
            double Credit = actiontype.ToLower() == "credit" ? Convert.ToDouble(amount) : 0;

            result = InstantPay_ApiService.LedgerDebitCredit(Convert.ToDouble(amount), agentid, agencyName, agentid, GetLocalIPAddress(), Debit, Credit, actiontype, Remark, Uploadtype, shortRemark, narration, ledtype);
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }
    #endregion
}