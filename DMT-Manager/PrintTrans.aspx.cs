﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using InstantPayServiceLib;

public partial class DMT_Manager_PrintTrans : System.Web.UI.Page
{
    private static string UserId { get; set; }
    private static string Mobile { get; set; }
    private static string RemitterId { get; set; }
    private static string BeneficaryId { get; set; }
    private static string TrackId { get; set; }
    public static string HtmlContent { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            UserId = Session["UID"].ToString();

            Mobile = Request.QueryString["mobile"] != null ? Request.QueryString["mobile"].ToString() : string.Empty;
            RemitterId = Request.QueryString["sender"] != null ? Request.QueryString["sender"].ToString() : string.Empty;
            BeneficaryId = Request.QueryString["beneficaryid"] != null ? Request.QueryString["beneficaryid"].ToString() : string.Empty;
            TrackId = Request.QueryString["trackid"] != null ? Request.QueryString["trackid"].ToString() : string.Empty;

            if (string.IsNullOrEmpty(Mobile) && string.IsNullOrEmpty(RemitterId) && string.IsNullOrEmpty(BeneficaryId) && string.IsNullOrEmpty(TrackId))
            {
                Response.Redirect("/");
            }
            else
            {
                HtmlContent = BindTransPrint();
            }
        }
        else
        {
            Response.Redirect("/");
        }
    }

    public string BindTransPrint()
    {
        StringBuilder sbResult = new StringBuilder();

        if (!string.IsNullOrEmpty(UserId))
        {
            if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
            {
                //DataTable dtAgency = InstantPayFun.GetAgencyDetailById(UserId);

                DataTable dtRemitter = InstantPay_ApiService.GetCombieRemitterDetail(UserId, RemitterId, Mobile);

                sbResult.Append("<div style='border: 1px solid #ff414d; width: 800px; border-collapse: initial;' class='main'>");
                sbResult.Append("<div class='col-sm-12'>");
                sbResult.Append("<a href='#'><img src='../Images/gallery/logo(ft).png' style='max-width: 200px;' /></a>");
                sbResult.Append("<span style='float: right!important; text-align: right; padding: 7px;'>");
                sbResult.Append("<span>" + dtRemitter.Rows[0]["FirstName"].ToString() + " " + dtRemitter.Rows[0]["LastName"].ToString() + "</span><br />");
                sbResult.Append("<span>" + dtRemitter.Rows[0]["Address"].ToString() + ", " + dtRemitter.Rows[0]["City"].ToString() + ", " + dtRemitter.Rows[0]["State"].ToString() + ", " + dtRemitter.Rows[0]["PinCode"].ToString() + "</span>");
                sbResult.Append("</span>");
                sbResult.Append("<hr style='text-align: center; border: 1px solid #ff414d; background-color: #ff414d; margin-top: 0px; margin-bottom: 0;' />");
                sbResult.Append("</div>");
                sbResult.Append("<div class='col-sm-12'>");
                sbResult.Append("<h4 class='heading'>Customer Transaction Receipt <div class='sendmail form-validation'><input type='text' placeholder='enter email id' id='ReceiptSendMail' style='width: 200px;' /> <span style='cursor:pointer;' id='btnMailSend'>Send Mail</span></div></h4>");
                sbResult.Append("<table data-toggle='table' style='width: 800px; border-collapse: initial; border: 1px solid #ff414d; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;'>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Name</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtRemitter.Rows[0]["FirstName"].ToString() + " " + dtRemitter.Rows[0]["LastName"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Mobile Number</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtRemitter.Rows[0]["Mobile"].ToString() + "</td>");
                sbResult.Append("</tr>");

                DataTable dtBen = InstantPay_ApiService.GetBenDetailsByRemitterId(RemitterId, UserId, BeneficaryId);

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Name</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["Name"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Account Number</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["Account"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Bank's IFSC</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["IfscCode"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Bank</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["Bank"].ToString() + "</td>");
                sbResult.Append("</tr>");

                DataTable dtTrans = InstantPay_ApiService.GetTransactionHistoryByTrackId(TrackId);

                int transCount = dtTrans.Rows.Count;

                if (transCount > 1)
                {
                    decimal totalamount = 0;
                    for (int t = 0; t < dtTrans.Rows.Count; t++)
                    {
                        totalamount = totalamount + Convert.ToDecimal(dtTrans.Rows[t]["Amount"].ToString());
                    }

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Amount</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;font-weight:bold;'>₹ " + totalamount + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr><th colspan='2'>&nbsp;</th><td colspan='2'>&nbsp;</td></tr>");
                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Order ID</th>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Track ID</th>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Amount</th>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Date &amp; Times</th>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Status</th>");
                    sbResult.Append("</tr>");

                    for (int t = 0; t < dtTrans.Rows.Count; t++)
                    {
                        sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                        sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[t]["orderid"].ToString() + "</td>");
                        sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[t]["TrackId"].ToString() + "</td>");
                        sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;font-weight:bold;'>₹ " + dtTrans.Rows[t]["Amount"].ToString() + "</td>");
                        sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[t]["UpdatedDate"].ToString()) + "</td>");
                        if (!string.IsNullOrEmpty(dtTrans.Rows[t]["Status"].ToString()) && dtTrans.Rows[t]["Status"].ToString().ToLower() == "transaction successful")
                        {
                            sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px; color:#28a745; font-weight:bold;'>" + dtTrans.Rows[t]["Status"].ToString() + "</td>");
                        }
                        else if (!string.IsNullOrEmpty(dtTrans.Rows[t]["Status"].ToString()))
                        {
                            sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px; color:#ff414d; font-weight:bold;'>" + dtTrans.Rows[t]["Status"].ToString() + "</td>");
                        }
                        else
                        {
                            sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px; color:#ff414d; font-weight:bold;'>Transaction Failed</td>");
                        }
                        sbResult.Append("</tr>");
                    }
                }
                else
                {
                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Order ID</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["orderid"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction ID</th>");
                    //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["ipay_id"].ToString() + "</td>");
                    //sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Track ID</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["TrackId"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Amount</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>₹ " + dtTrans.Rows[0]["Amount"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Charged Amount</th>");
                    //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["charged_amt"].ToString() + "</td>");
                    //sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Date & Times</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[0]["UpdatedDate"].ToString()) + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Status</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["Status"].ToString() + "</td>");
                    sbResult.Append("</tr>");
                }

                sbResult.Append("</table>");
                sbResult.Append("<h4 style='padding-left: 5px;'>Note :-</h4>");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>1. Customer transaction charge is minimum of Rs. 10/- and Maximum 1% of the transaction amount.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>2. In case of non-payment to the beneficiary, the customer will receive an SMS with an OTP that he/she needs to present at the agent location where the transaction was initiated.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>3. In case the Agent charges the Customer in excess of the fee/ charges as mentioned in the receipt, he/she should lodge complaint about the same with our Customer Care on Tel. No. 7409455555 or email us at fastgocash@gmail.com.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>4. The receipt is subject to terms and conditions, privacy policy and terms of use detailed in the website www.fastgocash.com and shall be binding on the Customer for each transaction.</p><br /><br />");

                sbResult.Append("<table data-toggle='table' style='width: 800px; border-collapse: initial; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<tr>");
                sbResult.Append("<td><p>Date..............................</p></td>");
                sbResult.Append("<td><p style='float: right!important;'>Signature Of Customer's......................................................</p></td>");
                sbResult.Append("</tr>");
                sbResult.Append("</table>");
                sbResult.Append("<br /><br />");
                sbResult.Append("</div>");
                sbResult.Append("</div>");
            }
        }

        return sbResult.ToString();
    }

    public static string ConvertStringDateToStringDateFormate(string date)
    {
        DateTime dtDate = new DateTime();

        if (!string.IsNullOrEmpty(date))
        {
            dtDate = DateTime.Parse(date);
            return dtDate.ToString("dd MMM yyyy hh:mm tt");
        }

        return string.Empty;
    }

    [WebMethod]
    public static string SendReceiptInMail(string emailid)
    {
        if (!string.IsNullOrEmpty(emailid.Trim()))
        {
            string mailBody = ReceiptSend();

            DataTable dtAgency = InstantPay_ApiService.GetAgencyDetailById(UserId);

            if (dtAgency != null && dtAgency.Rows.Count > 0)
            {
                string toEmail = dtAgency.Rows[0]["Email"].ToString();

                SqlTransactionDom STDOM = new SqlTransactionDom();
                DataTable MailDt = new DataTable();
                MailDt = STDOM.GetMailingDetails("MONEY_TRANSFER", UserId).Tables[0];
                if (MailDt != null && MailDt.Rows.Count > 0)
                {
                    bool Status = Convert.ToBoolean(MailDt.Rows[0]["Status"].ToString());
                    string subject = "Payment Transaction Receipt [Client Ref# " + TrackId + "]";
                    if (Status)
                    {
                        int isSuccess = STDOM.SendMail(toEmail, MailDt.Rows[0]["MAILFROM"].ToString(), MailDt.Rows[0]["BCC"].ToString(), MailDt.Rows[0]["CC"].ToString(), MailDt.Rows[0]["SMTPCLIENT"].ToString(), MailDt.Rows[0]["UserId"].ToString(), MailDt.Rows[0]["Pass"].ToString(), mailBody, subject, "");
                        if (isSuccess > 0)
                        {
                            return "sent";
                        }
                        else
                        {
                            return "failed";
                        }
                    }
                }
            }
        }

        return string.Empty;
    }

    private static string ReceiptSend()
    {
        StringBuilder sbResult = new StringBuilder();

        if (!string.IsNullOrEmpty(UserId))
        {
            if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
            {
                //DataTable dtAgency = InstantPayFun.GetAgencyDetailById(UserId);

                DataTable dtRemitter = InstantPay_ApiService.GetCombieRemitterDetail(UserId, RemitterId, Mobile);

                sbResult.Append("<div style='border: 1px solid #ff414d; width: 800px; border-collapse: initial;' class='main'>");
                sbResult.Append("<div class='col-sm-12'>");
                sbResult.Append("<a href='#'><img src='www.fly2njoy.com/Images/gallery/logo(ft).png' style='max-width: 200px;' /></a>");
                sbResult.Append("<span style='float: right!important; text-align: right; padding: 7px;'>");
                sbResult.Append("<span>" + dtRemitter.Rows[0]["FirstName"].ToString() + " " + dtRemitter.Rows[0]["LastName"].ToString() + "</span><br />");
                sbResult.Append("<span>" + dtRemitter.Rows[0]["Address"].ToString() + ", " + dtRemitter.Rows[0]["City"].ToString() + ", " + dtRemitter.Rows[0]["State"].ToString() + ", " + dtRemitter.Rows[0]["PinCode"].ToString() + "</span>");
                sbResult.Append("</span>");
                sbResult.Append("<hr style='text-align: center; border: 1px solid #ff414d; background-color: #ff414d; margin-top: 0px; margin-bottom: 0;' />");
                sbResult.Append("</div>");
                sbResult.Append("<div class='col-sm-12'>");
                sbResult.Append("<h4 style='text-align: center;'>Customer Transaction Receipt</h4>");
                sbResult.Append("<table data-toggle='table' style='width: 800px; border-collapse: initial; border: 1px solid #ff414d; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;'>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Name</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtRemitter.Rows[0]["FirstName"].ToString() + " " + dtRemitter.Rows[0]["LastName"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Mobile Number</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtRemitter.Rows[0]["Mobile"].ToString() + "</td>");
                sbResult.Append("</tr>");

                DataTable dtBen = InstantPay_ApiService.GetBenDetailsByRemitterId(RemitterId, UserId, BeneficaryId);

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Name</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["Name"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Account Number</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["Account"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Bank's IFSC</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["IfscCode"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Bank</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["Bank"].ToString() + "</td>");
                sbResult.Append("</tr>");

                DataTable dtTrans = InstantPay_ApiService.GetTransactionHistoryByTrackId(TrackId);

                int transCount = dtTrans.Rows.Count;

                if (transCount > 1)
                {
                    decimal totalamount = 0;
                    for (int t = 0; t < dtTrans.Rows.Count; t++)
                    {
                        totalamount = totalamount + Convert.ToDecimal(dtTrans.Rows[t]["Amount"].ToString());
                    }

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Amount</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;font-weight:bold;'>₹ " + totalamount + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr><th colspan='2'>&nbsp;</th><td colspan='2'>&nbsp;</td></tr>");
                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Order ID</th>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Track ID</th>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Amount</th>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Date &amp; Times</th>");
                    sbResult.Append("<th style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Status</th>");
                    sbResult.Append("</tr>");

                    for (int t = 0; t < dtTrans.Rows.Count; t++)
                    {
                        sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                        sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[t]["orderid"].ToString() + "</td>");
                        sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[t]["TrackId"].ToString() + "</td>");
                        sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;font-weight:bold;'>₹ " + dtTrans.Rows[t]["Amount"].ToString() + "</td>");
                        sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[t]["UpdatedDate"].ToString()) + "</td>");
                        if (!string.IsNullOrEmpty(dtTrans.Rows[t]["Status"].ToString()) && dtTrans.Rows[t]["Status"].ToString().ToLower() == "transaction successful")
                        {
                            sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px; color:#28a745; font-weight:bold;'>" + dtTrans.Rows[t]["Status"].ToString() + "</td>");
                        }
                        else if (!string.IsNullOrEmpty(dtTrans.Rows[t]["Status"].ToString()))
                        {
                            sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px; color:#ff414d; font-weight:bold;'>" + dtTrans.Rows[t]["Status"].ToString() + "</td>");
                        }
                        else
                        {
                            sbResult.Append("<td style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; padding: 10px; color:#ff414d; font-weight:bold;'>Transaction Failed</td>");
                        }
                        sbResult.Append("</tr>");
                    }
                }
                else
                {
                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Order ID</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["orderid"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction ID</th>");
                    //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["ipay_id"].ToString() + "</td>");
                    //sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Track ID</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["TrackId"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Amount</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>₹ " + dtTrans.Rows[0]["Amount"].ToString() + "</td>");
                    sbResult.Append("</tr>");

                    //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Charged Amount</th>");
                    //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["charged_amt"].ToString() + "</td>");
                    //sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Date & Times</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[0]["UpdatedDate"].ToString()) + "</td>");
                    sbResult.Append("</tr>");

                    sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                    sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Status</th>");
                    sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["Status"].ToString() + "</td>");
                    sbResult.Append("</tr>");
                }

                sbResult.Append("</table>");
                sbResult.Append("<h4 style='padding-left: 5px;'>Note :-</h4>");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>1. Customer transaction charge is minimum of Rs. 10/- and Maximum 1% of the transaction amount.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>2. In case of non-payment to the beneficiary, the customer will receive an SMS with an OTP that he/she needs to present at the agent location where the transaction was initiated.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>3. In case the Agent charges the Customer in excess of the fee/ charges as mentioned in the receipt, he/she should lodge complaint about the same with our Customer Care on Tel. No. 7409455555 or email us at fastgocash@gmail.com.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>4. The receipt is subject to terms and conditions, privacy policy and terms of use detailed in the website www.fastgocash.com and shall be binding on the Customer for each transaction.</p><br /><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>5. This is a system generated receipt hence does not require any signature.</p><br /><br />");
                sbResult.Append("</div>");
                sbResult.Append("</div>");
            }
        }

        return sbResult.ToString();
    }
}