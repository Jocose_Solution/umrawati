﻿
Partial Class MasterPagefor_money_transfer
    Inherits System.Web.UI.MasterPage
    Public AgencyName As String = ""
    Public AgencyId As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If Session("UID") <> "" AndAlso Session("UID") IsNot Nothing Then
                AgencyName = Session("AgencyName")
                AgencyId = Session("AgencyId")
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub Logout_Click(sender As Object, e As EventArgs)
        Try
            FormsAuthentication.SignOut()
            Session.Abandon()
            Response.Redirect("~/Login.aspx")
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
End Class

