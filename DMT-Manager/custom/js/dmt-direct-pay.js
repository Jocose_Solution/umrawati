﻿function ResetRegistrationForm() {
    $("#txtRegMobileNo").val("");
    $("#txtRegFirstName").val("");
    $("#txtRegLastName").val("");
    $("#txtRegPinCode").val("");
    $("#txtRegOtp").val("");
    $("#txtCurrLocalAddress").val("");    
    $("#perrormessage").html("");
}

function DMTDRemitterMobileSearch() {
    $("#btnRegOtpVarification").html("Verify");
    $("#DMTDTokenExpired").html("");
    ResetRegistrationForm();
    var thisbutton = $("#btnDMTDRemitterMobileSearch");
    if (CheckFocusBlankValidation("txtDMTDSenderMobileNo")) return !1;
    var mobileno = $("#txtDMTDSenderMobileNo").val();

    if (mobileno.length == 10) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/dmt-manager/dmt-direct.aspx/DMTDRemitterMobileSearch",
            data: '{mobileno: ' + JSON.stringify(mobileno) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d.length > 0) {
                    if (data.d[0] == "success") { window.location.href = data.d[1]; }
                    else if (data.d[0] == "otpsent") {
                        $("#txtDMTDSenderMobileNo").val("");
                        $("#otperrormessage").html("");
                        regmobile = data.d[1];
                        $("#hdnRegRemtId").val(data.d[2]);

                        $("#RegistSectionForm").addClass("hidden");
                        $("#OTPSectionForm").removeClass("hidden");
                        $("#RegOtpHeading").html("Sender already registered, Please verify sender.<br/>Otp sent successfully to your mobile number.");
                        $("#RegistMargin").removeAttr("style").css("margin", "8% auto");
                        $(".remitterreg").click();
                        $(thisbutton).html("Search");
                        StartCountDown(30);
                    }
                    else if (data.d[0] == "registration") {
                        ResetRegistrationForm();
                        $("#txtRegMobileNo").val(mobileno);
                        $(".remitterreg").click();
                        $(thisbutton).html("Search");
                        $("#txtDMTDSenderMobileNo").val("");
                    }
                    else if (data.d[0] == "error") {
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Failed !", "#ff414d", data.d[1], "#ff414d");
                        $(thisbutton).html("Search");
                    }
                    else if (data.d[0] == "reload") { window.location.reload(); }
                }
                else {
                    $("#DMTDTokenExpired").html("Error occured, Token expired !");
                }
                $(thisbutton).html("Search");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
    else {
        ShowMessagePopup("<i class='fa fa-exclamation-triangle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Failed !", "#ff414d", "Mobile number not valid.", "#ff414d");
    }
}

var regirsterremitter = null; var regmobile = null;
function SubmitRegRemitter() {
    $("#perrormessage").html("");
    $("#otperrormessage").html("");
    var thisbutton = $("#btnRemtrRegistration");


    if (CheckFocusBlankValidation("txtRegMobileNo")) return !1;
    var mobile = $("#txtRegMobileNo").val();
    if (mobile.length != 10) { $("#txtRegMobileNo").focus(); return false; }
    if (CheckFocusBlankValidation("txtRegFirstName")) return !1;
    if (CheckFocusBlankValidation("txtRegLastName")) return !1;
    if (CheckFocusBlankValidation("txtRegPinCode")) return !1;
    if (CheckFocusBlankValidation("txtCurrLocalAddress")) return !1;

    var firstname = $("#txtRegFirstName").val();
    var lastname = $("#txtRegLastName").val();
    var pincode = $("#txtRegPinCode").val();
    var localadd = $("#txtCurrLocalAddress").val();

    if (mobile.length == 10) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/dmt-manager/dmt-direct.aspx/DMTDRemitterRegistration",
            data: '{mobile: ' + JSON.stringify(mobile) + ',firstname: ' + JSON.stringify(firstname) + ',lastname: ' + JSON.stringify(lastname) + ',pincode: ' + JSON.stringify(pincode) + ',localadd: ' + JSON.stringify(localadd) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != null) {
                    if (data.d[0] == "success") {
                        $("#RegistMargin").removeAttr("style").css("margin", "10% auto");

                        $("#RegistSectionForm").addClass("hidden");
                        $("#OTPSectionForm").removeClass("hidden");
                        $("#hdnRegRemtId").val(data.d[1]);
                        regirsterremitter = data.d[1];
                        regmobile = mobile;
                        StartCountDown(30);
                    }
                    else if (data.d[0] == "failed") {
                        $("#perrormessage").html(data.d[1]);
                    }
                    else if (data.d[0] == "reload") {
                        window.location.reload();
                    }
                    else {
                        $("#btnregclose").click();
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Error Occurred !", "#d91717", data.d[1], "#d91717");
                    }
                }
                else {
                    $("#perrormessage").html("Error occured, Token expired !");
                }
                $(thisbutton).html("Register");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
    else {
        $("#perrormessage").html("Mobile number should be 10 digits!");
    }
}

function SubmitRegOtpVerification() {
    $("#otperrormessage").html("");
    var thisbutton = $("#btnRegOtpVarification");
    if (CheckFocusBlankValidation("txtRegOtp")) return !1;

    var mobile = regmobile;
    var enteredotp = $("#txtRegOtp").val();
    var hdnremtVal = $("#hdnRegRemtId").val();
    var remitterid = hdnremtVal != "" ? hdnremtVal : regirsterremitter;

    if (mobile != "" && enteredotp != "" && remitterid != "") {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/dmt-manager/dmt-direct.aspx/DMTDRemitterVarification",
            data: '{mobile: ' + JSON.stringify(mobile) + ',remitterid: ' + JSON.stringify(remitterid) + ',otp: ' + JSON.stringify(enteredotp) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != null) {
                    if (data.d[0] == "success") {
                        $("#btnregformclose").click();
                        window.location.href = data.d[1];
                        //ShowMessagePopup("<i class='fa fa-check-circle text-success' aria-hidden='true'></i>&nbsp;Success", "#28a745", "Remitter registration has been successfully completed.", "#28a745");
                    }
                    else if (data.d[0] == "failed") {
                        $("#otperrormessage").html(data.d[1]);
                    }
                    else {
                        $("#btnregformclose").click();
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Error Occurred !", "#d91717", data.d[1], "#d91717");
                    }
                }
                $(thisbutton).html("Verify");
                $("#txtRegOtp").val("");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

var _tick = null;
function StartCountDown(startSecond) {
    $("#btnResendOTP").addClass("hidden");
    $("#timersection").removeClass("hidden");

    clearInterval(_tick);
    $("#secRemaing").html("");
    $("#minRemaing").html("");
    $(".strcountdown").removeClass("hidden");
    var remSeconds = startSecond;
    var secondCounters = remSeconds % 60;
    function formarteNumber(number) { if (number < 10) return '0' + number; else return '' + number; }
    function startTick() {
        $("#secRemaing").text(formarteNumber(secondCounters));
        $("#minRemaing").text(formarteNumber(parseInt(remSeconds / 60)));
        if (secondCounters == 0) { secondCounters = 60; }
        _tick = setInterval(function () {
            if (remSeconds > 0) {
                remSeconds = remSeconds - 1;
                secondCounters = secondCounters - 1;
                $("#secRemaing").text(formarteNumber(secondCounters));
                $("#minRemaing").text(formarteNumber(parseInt(remSeconds / 60)));
                if (secondCounters == 0) { secondCounters = 60; }
            }
            else {
                clearInterval(_tick);
                ResetResend();
            }
        }, 1000);
    }
    startTick();
}

function ResetResend() {
    $("#btnResendOTP").removeClass("hidden");
    $("#timersection").addClass("hidden");
}

function ResendOTP() {
    $("#otperrormessage").html("");
    $("#btnResendOTP").html("Wait... <i class='fa fa-pulse fa-spinner'></i>");
    var hdnremtVal = $("#hdnRegRemtId").val();

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/dmt-manager/dmt-direct.aspx/DMTDResendOtpToRemitterMobile",
        data: '{mobileno: ' + JSON.stringify(regmobile) + ',remitterid: ' + JSON.stringify(hdnremtVal) + '}',
        datatype: "json",
        success: function (data) {
            if (data.d != null) {
                if (data.d[0] == "otpsent") {
                    $("#otperrormessage").html("OTP sent.");
                    StartCountDown(30);
                }
                else {
                    $("#otperrormessage").html(data.d[1]);
                }
            }
            $("#btnResendOTP").html("Resend OTP");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}